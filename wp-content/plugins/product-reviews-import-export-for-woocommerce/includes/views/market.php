<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="market-box table-box-main">
    <?php /* <div class="getting-started-video">
        <h2><?php _e('Watch getting started video', 'product-reviews-import-export-for-woocommerce');?></h2>
    <iframe src="https://www.youtube.com/embed/L-01qI1EZWE?rel=0&showinfo=0" frameborder="0" allowfullscreen="allowfullscreen" align="center"></iframe>
    </div> */ ?>
    <div class="pipe-review-widget">
        <?php
        echo sprintf(__('<div class=""><p><i>If you like the plugin please leave us a %1$s review!</i><p></div>', 'product-reviews-import-export-for-woocommerce'), '<a href="https://wordpress.org/support/plugin/product-reviews-import-export-for-woocommerce/reviews?rate=5#new-post" target="_blank" class="xa-pipe-rating-link" data-reviewed="' . esc_attr__('Thanks for the review.', 'product-reviews-import-export-for-woocommerce') . '">&#9733;&#9733;&#9733;&#9733;&#9733;</a>');
        ?>
    </div>
    <div class="pipe-premium-features">
        <ul style="font-weight: bold; color:#666; list-style: none; background:#f8f8f8; padding:20px; margin:20px 15px; font-size: 15px; line-height: 26px;">
                <li style=""><?php echo __('30 Day Money Back Guarantee','cookie-law-info'); ?></li>
                <li style=""><?php echo __('Fast and Superior Support','cookie-law-info'); ?></li>
                <li style="">
                    <a href="https://www.webtoffee.com/product/product-import-export-woocommerce/?utm_source=free_plugin_sidebar&utm_medium=Review_imp_exp_basic&utm_campaign=Product_Import_Export&utm_content=<?php echo WF_PR_REV_IMP_EXP_VERSION; ?>" target="_blank" class="button button-primary button-go-pro"><?php _e('Upgrade to Premium', 'product-reviews-import-export-for-woocommerce'); ?></a>
                </li>
            </ul>
            <span>
        <ul class="ticked-list">
            <li><?php _e('Additional data filters for exporting reviews, by ratings, products, verified customer only and more.', 'product-reviews-import-export-for-woocommerce');?></li>
            <li><?php _e('Import only selective columns.', 'product-reviews-import-export-for-woocommerce');?></li>
            <li><?php _e('Map fields during import.', 'product-reviews-import-export-for-woocommerce');?></li>
            <li><?php _e('Manipulate/evaluate data during import.', 'product-reviews-import-export-for-woocommerce');?> </li>
            <li><?php _e('Import/Export options via FTP.', 'product-reviews-import-export-for-woocommerce');?></li>
            <li><?php _e('Schedule automatic import and export.', 'product-reviews-import-export-for-woocommerce');?></li>
            <li><?php _e('Premium support.', 'product-reviews-import-export-for-woocommerce');?></li>         
        </ul>
    </span>
    <center> 
        <a href="https://www.webtoffee.com/category/documentation/product-import-export-plugin-for-woocommerce/" target="_blank" class="button button-doc-demo"><?php _e('Documentation', 'product-reviews-import-export-for-woocommerce'); ?></a></center>
    </div>
    
    </div>
