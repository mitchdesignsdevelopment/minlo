<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

use JetBrains\PhpStorm\Language;

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
global $language;
$term = get_queried_object();
$id = "";
$term_slug = "";
$parent_name = "";
if($term){
    $id = $term->term_id;
    $term_slug = $term->slug;
    $parent = $term->parent;
    $parent_name = ($parent)? get_term_by('id',$parent,'product_cat'):'';
    $cat_slug = ($term->taxonomy == 'product_cat')? $term->slug : $term->term_id;
    $page_type = $term->taxonomy;
}
$posts_per_page = 20;

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
//do_action( 'woocommerce_before_main_content' );
?>
    <!-- Start Page Content -->
<div class="page-content shop-page">
	<div class="container">
	  <div class="page-head">
          <div class="title">
            <span><?php echo($language=="ar")?'اكتشف و تسوق':'Discover & Shop';?></span>
            <h1><?php echo($language=="ar")? get_field('ar_title',$term) : woocommerce_page_title();?>
			</h1>
          </div>
		  <?php
		$get_brands = get_terms( 'brand',array(
			'hide_empty' => true,
			'fields' => 'ids',
		) );
		$brands = get_terms( 'brand',array(
			'hide_empty' => true,
		) );
		
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 1,
			'post_status' => 'publish',
			'product_cat' => $term->slug,
			'relation' => 'AND',
			'tax_query' => array(
				'relation' => 'AND',
				array(
				'taxonomy' => 'brand',
				'field' => 'id',
				'terms' => $get_brands
			)
			)
		);
		$check_brands = get_posts( $args );
		if($brands && $check_brands):
			
		?>
			<ul class="shop-filter">
				<?php
				$count=0;
				foreach($brands as $brand){
					if(!is_shop()){
						$args = array(
						    'post_type' => 'product',
						    'posts_per_page' => 1,
						    'post_status' => 'publish',
						    'product_cat' => $term->slug,
						    'relation' => 'AND',
						    'tax_query' => array(
						        'relation' => 'AND',
						        array(
						        'taxonomy' => 'brand',
						        'field' => 'slug',
						        'terms' => array($brand->slug)
						    )
						    )
						);
						$products_in_brand = count(get_posts( $args ));
					}else{
						$products_in_brand = 1;
					}
					if($products_in_brand){
						?>
						<li>
							<input
							type="checkbox"
							class="filled-in filter_input filter-brand"
							value="<?php echo $brand->slug;?>"
							id="checkbox-brand<?php echo $count;?>"
							data-target="checkbox-brand<?php echo $count;?>"
							<?php echo (isset($_GET['brands'])&&in_array($brand->slug, explode(",",$_GET['brands'])))?'checked':''; ?>/>
							<label class="checkbox-brand" for="checkbox-brand<?php echo $count;?>"><span><?php echo ($language == 'en')?  get_field('ar_title',$brand) : $brand->name;?></span></label>
						</li>
						<?php
						$count++;
					}
				}
				?>
			</ul>
		<?php endif;?>
        </div>
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	//do_action( 'woocommerce_before_shop_loop' );

	//woocommerce_product_loop_start();
	?>
	<div class="products-list products" data-slug="<?php echo (is_shop())? '':$cat_slug; ?>" data-type="<?php echo (is_shop())?'shop':$term->taxonomy; ?>" data-count="20" data-page="1" data-posts="0" data-search="<?php echo $_GET['s']?>" data-sort="<?php echo (isset($_GET['orderby']))? $_GET['orderby'] :'featured'; ?>" data-lang="<?php echo $language; ?>">
			<div class="col description wow fadeIn" data-wow-delay=".6s" data-wow-offset="0">
				<p><?php echo($language=="ar")? get_field('ar_description'):$term->description; ?></p>
            </div>
	<div class="links" >
			<?php
			
			if ( wc_get_loop_prop( 'total' ) ) {
				while ( have_posts() ) {
					the_post();

					/**
					 * Hook: woocommerce_shop_loop.
					 */
					do_action( 'woocommerce_shop_loop' );

					wc_get_template_part( 'content', 'product' );
				}
			}

			//woocommerce_product_loop_end();

			/**
			 * Hook: woocommerce_after_shop_loop.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			//do_action( 'woocommerce_after_shop_loop' );
			?>
	</div>
	<div class="spinner" data-slug="<?php echo (is_shop())? '':$cat_slug; ?>" 
                        data-type="<?php echo (is_shop())?'shop':$term->taxonomy; ?>" 
                        data-count="20" data-page="1" data-posts="0" 
                        data-sort="<?php echo (isset($_GET['orderby']))? $_GET['orderby'] :'featured'; ?>">
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                    </div>
			</div>
				<?php
			} else {
				/**
				 * Hook: woocommerce_no_products_found.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
			}
			?>
	</div>
</div>
<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
//do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
