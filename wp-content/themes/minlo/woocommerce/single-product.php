<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header('shop');
global $product,$language;
$terms = get_the_terms( get_the_ID(), 'product_cat' );
$x = get_queried_object();
$product = wc_get_product($x->ID);
?>

<div class="page-content single-product" data-product_title="<?php the_title(); ?>" data-product_id="<?php echo get_the_ID()?>" data-price="<?php echo json_encode($product->get_price()); ?>" data-cat="<?php echo $terms[0]->name; ?>">
	<div class="container">
		<div class="row">
				<div class="page-title">
						<?php
							$count=0;
							if($terms){
								foreach($terms as $term){
									if($count==0){
										if($term->parent==0): //here i hidden the black friday cat
									?>
										<a class="wow fadeInUp" date-wow-delay="5s" href="<?php echo get_term_link($term); echo($language == 'ar')?"?lang=ar":" "?>"><?php echo($language == 'ar')? get_field('ar_title',$term) : $term->name;?></a>
									<?php
									$count++;
									endif;
									}
								}
							}
						?>
					<h1 class="wow fadeInUp" date-wow-delay="5.5s"><?php echo($language=="ar")? get_field('ar_title'):get_the_title();?></h1>
				</div>

			<div class="page-container">
				<div class="product-banner wow fadeIn" date-wow-delay="10s" style="background: <?php echo get_field('hero_background_color');?> !important;">
					<div class="product-banner-gallery">
						<?php if(have_rows('hero_banners')): while(have_rows('hero_banners')): the_row();?>
						<div class="item">
							<div class="item-container">
								<div class="content <?php echo get_sub_field('text_color');?>">
									<h3 class="wow fadeInUp" date-wow-delay="3s"><?php echo($language=="ar")?get_sub_field('title_ar'):get_sub_field('title');?></h3>
									<p class="wow fadeInUp" date-wow-delay="3.5s"><?php echo($language=="ar")?get_sub_field('subtitle_ar'):get_sub_field('subtitle');?></p>
								</div>
								<div class="image">
									<img src="<?php echo get_sub_field('image');?>" class="wow fadeInRight" date-wow-delay="2.5s" alt="" />
									<?php if(get_sub_field('style')):?>
									<span style="<?php echo get_sub_field('style');?>" class="wow fadeInUp" date-wow-delay="2.8s"><?php echo get_sub_field('order');?></span>
									<?php endif;?>
								</div>
							</div>
						</div>
						<?php endwhile; endif;?>
					</div>
				</div>
					<?php if(have_rows("hero_images_&_description")&& !wp_is_mobile()): $count=1; $count_animation = 0.1;?>
					<div class="center-products  active">
						<?php while(have_rows("hero_images_&_description")): the_row(); 
							$video_webm  = get_sub_field('video_webm'); // WEBM Field Namee
						?>
						<div class="item <?php echo($count%2==0)?'flip':'';?>">
							<!-- <div class="image wow fadeInUp" data-wow-offset="20" data-wow-delay="<?php echo $count_animation;?>s">
								<img src="<?php //echo get_sub_field('image');?>" alt="image" />
							</div> -->
							<div class="image wow fadeInUp" data-wow-offset="20" data-wow-delay="<?php echo $count_animation;?>s">
							<video  data-wf-ignore="true" loop width="459" height="430" class="player__video viewer" autoplay muted playsinline>
							<source src="<?php echo $video_webm;?>" data-wf-ignore="true" />
							</video>
							<?php
											
							//echo do_shortcode('[video webm="'.$video_webm.'" autoplay="on" loop="on" muted preload="auto" width="459" height="440"]');
							// Build the  Shortcode
							// $attr =  array(
							// 'webm'     => $video_webm,
							// 'width' => '459',
							// 'height' => '441',
							// 'preload'  => 'auto',
							// 'autoplay' => 'on',
							// 'muted'=>"true",
							// );
							
							// Display the Shortcode
							// echo wp_video_shortcode(  $attr );
							?>
							</div>
							<div class="content">
								<span class="wow fadeInUp" data-wow-offset="10" data-wow-delay="0.2s"><?php echo($language=="ar")? get_sub_field('title_ar'):get_sub_field('title');?></span>
								<p class="wow fadeInUp" data-wow-offset="10" data-wow-delay="0.3s">
								<?php echo($language=="ar")? get_sub_field('description_ar'):get_sub_field('description');?>
								</p>
							</div>
						</div>
						<?php $count+=0.4; $count++; endwhile;?>
					</div>
					<?php endif;?>

				<div class="product-banners">
					<?php
					$gallery = get_field('gallery');
					if($gallery): $count=0.1; $ctr = 1;
					foreach($gallery as $image){
						//if($ctr==2):
					?>
						<!-- <div class="image text-box wow fadeInUp" data-wow-offset="0" data-wow-delay="<?php //echo $count;?>s" data-wow-duration="1s">
							<p><?php //echo($language=="ar")?get_field('ar_title'):get_the_title();?></p>
							<strong><?php //echo($language=="ar")?get_field('middle_image_title_ar'):get_field('middle_image_title');?></strong>
							<span><?php //echo($language=="ar")?get_field('middle_image_subtitle_ar'):get_field('middle_image_subtitle');?></span>
						</div> -->
					<?php //else: ?>
						<a href="#" class="image wow fadeInUp" data-wow-offset="0" data-wow-delay="<?php echo $count;?>s" data-wow-duration="1s">
						<img src="<?php echo $image;?>" alt="" />
						</a>
					<?php $ctr++; $count+=0.1; } endif;?>
				</div>
				<div class="single-product-con wow fadeInUp" data-wow-offset="0">
					<?php
						/**
						 * woocommerce_before_main_content hook.
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						//do_action( 'woocommerce_before_main_content' );
					?>
				
					<?php while ( have_posts() ) : ?>
						<?php the_post(); ?>

						<?php wc_get_template_part( 'content', 'single-product' ); ?>

					<?php endwhile; // end of the loop. ?>
				</div>
				<!-- Scroll To Top Button -->
				<div class="scroll-top wow fadeInUp mobile" data-wow-offset="0">
					<span class="moveTop"
					><i class="material-icons">expand_less</i></span
					>
					<b><?php echo($language=="ar")?'اذهب للأعلى':'Go up';?></b>
				</div>

				<?php if(!get_field('hide_pdf')):?>
					<div class="mobile-pdf">
						<div class="pdf-widget wow fadeInUp" data-wow-offset="0"  data-wow-delay="1s" >
							<div class="left-col">
								<div class="image">
									<img src="<?php echo get_field('pdf_image');?>" alt="image" width="43" height="48"/>
								</div>
								<div class="con">
									<span><?php echo($language=="ar")? get_field('pdf_title_ar'):get_field('pdf_title');?></span>
									<p><?php echo($language=="ar")?get_field('ar_title'):get_the_title();?></p>
								</div>
							</div>
							<div class="right-col">
								<a target="_blank" href="<?php echo get_field('pdf_button_url');?>" class="btn">
									<?php //echo($language=="ar")? get_field('pdf_button_title_ar'):get_field('pdf_button_title');?>
									<img src="<?php echo get_template_directory_uri().'/assets/images/Arrow-shape.png';?>" alt="" width="20" height="25">
								</a>
							</div>
						</div>
					</div>
				<?php endif;?>

			</div>
				<?php
				/**
				 * Hook: woocommerce_after_single_product_summary.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' );
				?>
		</div>
	</div>
</div>

<?php
get_footer('shop');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
