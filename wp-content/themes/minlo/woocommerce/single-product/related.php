<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $product,$language;

if ( $related_products ) : ?>

	<section class="related products more-products">

		<?php
		$heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'Related products', 'woocommerce' ) );

		if ( $heading ) :
			?>
			<h4><?php echo($language=="ar")?'منتجات مشابهة':'Related Products'; ?></h4>
		<?php endif; ?>
		
		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>

					<?php
					$post_object = get_post( $related_product->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

					wc_get_template_part( 'content', 'product' );
					?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</section>
	<?php
endif;
$current_product_id = $product->get_id();

$cats_array = array(0);

// get categories
$terms = wp_get_post_terms( $product->id, 'product_cat' );

// select only the category which doesn't have any children
foreach ( $terms as $term ) {
  $children = get_term_children( $term->term_id, 'product_cat' );
  if ( !sizeof( $children ) )
  $cats_array[] = $term->term_id;
}
$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type' => 'product',
	'post_status' => 'publish',
	'post__not_in' => array( $current_product_id ),   // exclude current product
	'posts_per_page' => 5,
	'tax_query' => array(
	  array(
		  'taxonomy' => 'product_cat',
		  'field' => 'id',
		  'terms' => $cats_array
	  ),
	),
	   'meta_query' => array(
        'relation' => 'AND',
        array(
           'key' => '_stock_status',
           'value' => 'instock',
           'compare' => 'IN'
        )
    )
  ));
  
$related_products = new WP_Query( $args );
if ( $related_products ) : ?>

	<section class="related products more-products you-may-interested">

		<?php
		$heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'Related products', 'woocommerce' ) );

		if ( $heading ) :
			?>
			<h4><?php echo($language=="ar")?'تسوق اكثر':'You May Interested In'; ?></h4>
		<?php endif; ?>
		<?php woocommerce_product_loop_start(); ?>
<?php
		
				while ($related_products->have_posts()) : $related_products->the_post();
				global $product; 
				wc_get_template_part( 'content', 'product' );
				endwhile; 
				wp_reset_postdata();
		?>
		<?php woocommerce_product_loop_end(); ?>
	</section>
	<?php
endif;

wp_reset_postdata();
