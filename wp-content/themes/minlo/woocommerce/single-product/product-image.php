<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . ( $post_thumbnail_id ? 'with-images' : 'without-images' ),
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	)
);
?>
<div class="product-slider-container">
	<?php
	$attachment_ids = $product->get_gallery_image_ids();
	if(!empty($attachment_ids)){
		if ( $attachment_ids && $product->get_image_id() ) {
	?>
	<div class="product-slider">
		<?php 
		$i=1; foreach ( $attachment_ids as $attachment_id ) {
			$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
			$thumbnail       = wp_get_attachment_image_src( $attachment_id, 'full' );
			$attributes      = array(
				'title'                   => get_post_field( 'post_title', $attachment_id ),
				'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
				'data-src'                => $full_size_image[0],
				'data-large_image'        => $full_size_image[0],
				'data-large_image_width'  => $full_size_image[1],
				'data-large_image_height' => $full_size_image[2],
			);
			?>
			<div class="slide-item">
				<img src="<?php echo esc_url( $full_size_image[0] ) ?>" alt="minlo - image">
			</div>
		<?php $i++; } ?>
	</div> 
	<div class="slider-nav">
		<?php $i=1; foreach ( $attachment_ids as $attachment_id ) {
			// $full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
			$full_size_image = wp_get_attachment_image_src( $attachment_id, array('750','600') );
			// $thumbnail       = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
			$thumbnail       = wp_get_attachment_image_src( $attachment_id,array('87','88'));

			$attributes      = array(
				'title'                   => get_post_field( 'post_title', $attachment_id ),
				'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
				'data-src'                => $full_size_image[0],
				'data-large_image'        => $full_size_image[0],
				'data-large_image_width'  => $full_size_image[1],
				'data-large_image_height' => $full_size_image[2],
			);
			?>
			<div class="item" data-thumb="<?php echo $full_size_image[0]; ?>">
				<img src="<?php echo esc_url( $thumbnail[0] ); ?>" alt="<?php echo get_the_title(get_the_ID()); ?>" class="image-<?php echo $i; ?>">
			</div>
		<?php $i++; } ?>
	</div>
	<?php }	}
	else{
	?>
	<div class="image" style="width: 100%; height:100%">
		<img style="width: 100%; height:100%" data-src="<?php echo 'http://139.162.196.12:8090/wp-content/uploads/woocommerce-placeholder.png';?>" alt="image">
	</div>
	<?php
	} 
	?>
</div>