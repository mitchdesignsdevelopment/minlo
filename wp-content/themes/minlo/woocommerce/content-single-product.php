<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product, $language;
$terms = get_the_terms(get_the_ID(), 'product_cat');


/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
//do_action( 'woocommerce_before_single_product' );

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>
<!-- <div id="product-<?php //the_ID(); 
                        ?>" data-class="new-single" <?php //wc_product_class( '', $product ); 
                                                    ?>> -->

<div class="left">
    <?php
    /**
     * Hook: woocommerce_before_single_product_summary.
     *
     * @hooked woocommerce_show_product_sale_flash - 10
     * @hooked woocommerce_show_product_images - 20
     */
    do_action('woocommerce_before_single_product_summary');
    ?>
    <?php //if (!wp_is_mobile()) : 
    ?>
    <div class="desktop-content-videos">
        <?php if (have_rows("videos")) : ?>
            <div class="vidoes-section">
                <?php while (have_rows("videos")) : the_row(); ?>
                    <div class="video" defer>
                        <!-- <video id="player" class="video-js vjs-theme-forest" data-setup="{ratio:1.9}" controls autoplay muted loop>
						<source src="<?php //echo get_sub_field('video'); ?>" type='video/mp4' />
						</video> -->
                        <?php echo get_sub_field('video'); ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

        <div class="reviews-info-tabs">
            <ul class="tabs">
                <li class="active" id="trigger-info">
                    <span><?php echo ($language == "ar") ? 'معلومات عن المنتج' : 'Product Information'; ?></span>
                </li>
                <li id="trigger-reviews">
                    <span><?php echo ($language == "ar") ? 'تقييمات' : 'Reviews'; ?></span>
                </li>
            </ul>
            <div class="content">
                <div class="tab-content review-section">
                    <div class="product-review-content">
                        <div class="pt-content review-section">

                            <div class="comments-container">
                                <?php
                                $user = wp_get_current_user();
                                if(!wp_is_mobile()){
                                if (get_comments_number()) : ?>
                                    <div class="right">
                                        <?php
                                        $five_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '5',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $four_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '4',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $three_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '3',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $two_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '2',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $one_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '1',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $comments_count = count($five_rating_comments) + count($four_rating_comments) + count($three_rating_comments) + count($two_rating_comments) + count($one_rating_comments);

                                        $current_user = wp_get_current_user();

                                        $rating  = $product->get_average_rating();
                                        $count   = $product->get_rating_count();
                                        //echo wc_get_rating_html( $rating, $count );
                                        $five_rate_number = count($five_rating_comments) / $comments_count * 100;
                                        $four_rate_number = count($four_rating_comments) / $comments_count * 100;
                                        $three_rate_number = count($three_rating_comments) / $comments_count * 100;
                                        $two_rate_number = count($two_rating_comments) / $comments_count * 100;
                                        $one_rate_number = count($one_rating_comments) / $comments_count * 100;
                                        ?>
                                        <div class="reviews-box">
                                            <div class="top">
                                                <h5><?php echo ($language == "") ? 'Product Review' : 'تقييم المنتج' ?></h5>
                                                <h6><?php echo ($language == '') ? get_field('page_title_en') : the_title();  ?>
                                                </h6>
                                            </div>
                                            <div class="bottom">
                                                <div class="right">
                                                    <b><?php echo ($language == '') ? floor($rating) . ' from 5 stars' : floor($rating) . ' من 5 نجوم' ?></b>
                                                    <div class="stars">
                                                        <?php for ($i = 1; $i <= floor($rating); $i++) : ?>
                                                            <span class="active"></span>
                                                        <?php endfor; ?>
                                                        <?php for ($i = floor($rating) + 1; $i <= 5; $i++) : ?>
                                                            <span></span>
                                                        <?php endfor; ?>
                                                    </div>
                                                    <p><?php echo ($language == '') ? get_comments_number() . ' Review' : get_comments_number() . ' تقييم' ?>
                                                    </p>
                                                </div>
                                                <ul>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '5 Stars' : '5 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $five_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($five_rate_number)) ? round($five_rate_number) : $five_rate_number; ?>%</span>
                                                    </li>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '4 Stars' : '4 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $four_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($four_rate_number)) ? round($four_rate_number) : $four_rate_number; ?>%</span>
                                                    </li>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '3 Stars' : '3 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $three_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($three_rate_number)) ? round($three_rate_number) : $three_rate_number; ?>%</span>
                                                    </li>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '2 Stars' : '2 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $two_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($two_rate_number)) ? round($two_rate_number) : $two_rate_number; ?>%</span>
                                                    </li>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '1 Stars' : '1 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $one_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($one_rate_number)) ? round($one_rate_number) : $one_rate_number; ?>%</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                        //if($current_user->ID == 0): 
                                        ?>
                                        <!-- <span class="checkout-login"><?php echo ($language == "") ? 'Leave Comment' : 'أكتب تقييم' ?></span> -->
                                        <?php //endif; 
                                        ?>
                                    </div>

                                    <div class="left">
                                        <p class="review-success" style="display: none;">
                                            <?php echo ($language == "") ? 'Thank you, your review is waiting for approval' : 'شكرا لك ، رأيك في انتظار الموافقة'; ?>
                                        </p>
                                        <?php //if($current_user->ID == 0): 
                                        ?>
                                        <!-- <h3>
													<?php echo ($language == '') ? 'In your opinion, how much is the product worth?' : 'في رأيك، كام يستحق المنتج؟'; ?>
												</h3> -->
                                        <?php //endif; 
                                        ?>
                                        <div id="comments" data-user="<?php echo get_current_user_id(); ?>" data-user-name="<?php echo $user->first_name . ' ' . $user->last_name; ?>" data-user-email="<?php echo $user->user_email; ?>">
                                            <?php
                                            if (comments_open() || get_comments_number()) :
                                                comments_template('/comments.php');
                                            endif;
                                            ?>
                                        </div>
                                    </div>
                                <?php else : ?>

                                    <div class="right no-rev">
                                        <div class="reviews-box">
                                            <div class="top">
                                                <h5><?php echo ($language == "") ? 'Product Review' : 'تقييم المنتج' ?></h5>
                                                <h6><?php echo ($language == '') ? get_field('page_title_en') : the_title();  ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="left no-rev">
                                        <p class="review-success" style="display: none;">
                                            <?php echo ($language == "") ? 'Thank you, your review is waiting for approval' : 'شكرا لك ، رأيك في انتظار الموافقة'; ?>
                                        </p>
                                        <div id="comments" data-user="<?php echo get_current_user_id(); ?>" data-user-name="<?php echo $user->first_name . ' ' . $user->last_name; ?>" data-user-email="<?php echo $user->user_email; ?>">
                                            <?php
                                            if (comments_open() || get_comments_number()) :
                                                comments_template('/comments.php');
                                            endif;
                                            ?>
                                        </div>
                                        <?php if (get_comments_number() == 0) : ?>
                                            <p class="noreviews">
                                                <?php echo ($language == "") ? 'There are no reviews on this product yet, you can start writing your first review' : 'لا يوجد تقييمات على هذا المنتج بعد، يمكنك البدء بكتابة أول تقييم' ?>
                                            </p>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; }?>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-content product-info active">
                    <h4><?php echo ($language == "ar") ? get_field('ar_title') : get_the_title(); ?></h4>
                    <div class="height2">
                        <?php echo ($language == "ar") ? get_field('ar_description') : apply_filters('the_content', get_the_content('', false)); ?>
                    </div>
                    <span id="readMore"><?php echo ($language == 'ar') ? 'اقرا المزيد' : ' Read More' ?> </span>
                    <span id="readLess" style="display:none"><?php echo ($language == 'ar') ? 'اقرا اقل' : 'Read Less' ?></span>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="right">
    <div class="description">

        <?php
        $count = 0;
        if ($terms) {
            foreach ($terms as $term) {
                if ($count == 0) {
                    if ($term->parent == 0) : //here i hidden the black friday cat
        ?>
                        <a class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s" href="<?php echo get_term_link($term);
                                                                                                echo ($language == 'ar') ? "?lang=ar" : " " ?>"><?php echo ($language == 'ar') ? get_field('ar_title', $term) : $term->name; ?></a>
        <?php
                        $count++;
                    endif;
                }
            }
        }
        ?>
       
        
        <h4 class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.2s">
            <?php echo ($language == "ar") ? get_field('ar_title') : get_the_title(); ?></h4>
        <!-- Content -->
        <p class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
            <?php echo get_field('description_above_pdf'); ?>
        </p>
        <!-- Specifications -->
    </div>
    <?php $colors = get_field('colors');
    if ($colors) : ?>
        <div class="color-variations">
            <h4><?php echo ($language == "ar") ? get_field('colors_title_ar') : get_field('colors_title'); ?></h4>
            <?php foreach ($colors as $color_attribute) : ?>
                <span style="background: <?php echo $color_attribute['hex_code']; ?>;"></span>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php
    $specifications = get_field('specifications');
    if ($specifications) : $count = 0.4; ?>
        <div class="product-features">
            <ul>
                <?php foreach ($specifications as $image) { ?>
                    <li class="wow fadeInUp" data-wow-offset="0" data-wow-delay="<?php echo $count; ?>s">
                        <img src="<?php echo $image; ?>" alt="image" />
                    </li>
                    <?php
                    //echo($language=="ar")? get_sub_field('title_ar'):get_sub_field('title');
                    ?>
                <?php $count += 0.1;
                } ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php
    if (have_rows("shopping_logos")) : $count = 0.6; ?>
        <div class="stores">
            <ul>
                <?php while (have_rows("shopping_logos")) : the_row(); ?>
                    <li class="wow fadeInUp" data-wow-offset="0" data-wow-delay="<?php echo $count; ?>s">
                        <span><?php echo ($language == "ar") ? get_sub_field('title_ar') : get_sub_field('title'); ?></span>
                        <a href="<?php echo get_sub_field('url'); ?>"><img src="<?php echo get_sub_field('image'); ?>" alt="image" /></a>
                    </li>
                <?php $count += 0.1;
                endwhile; ?>
            </ul>
        </div>
    <?php endif; ?>
    <!-- PDF Section -->
    <?php if (!get_field('hide_pdf')) : ?>
        <div class="desktop-pdf">
            <div class="pdf-widget wow fadeInUp " data-wow-offset="0" data-wow-delay="1s">
                <div class="left-col">
                    <div class="image">
                        <img src="<?php echo get_field('pdf_image'); ?>" alt="image" />
                    </div>
                    <div class="con">
                        <span><?php echo ($language == "ar") ? get_field('pdf_title_ar') : get_field('pdf_title'); ?></span>
                        <p><?php echo ($language == "ar") ? get_field('ar_title') : get_the_title(); ?></p>
                    </div>
                </div>
                <div class="right-col">
                    <a target="_blank" href="<?php echo get_field('pdf_button_url'); ?>" class="btn">
                        <?php //echo($language=="ar")? get_field('pdf_button_title_ar'):get_field('pdf_button_title');
                        ?>
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/Arrow-shape.png'; ?>" alt="">
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php //else : 
    ?>

    <?php if (have_rows("hero_images_&_description")) : $count = 1;
        $count_animation = 0.1; ?>
        <div class="center-products mobile">
            <?php while (have_rows("hero_images_&_description")&& wp_is_mobile()) : the_row();
                $video_webm  = get_sub_field('video_webm'); // WEBM Field Namee
                ?>
                <div class="item <?php echo ($count % 2 == 0) ? 'flip' : ''; ?>">
                    <div class="abdo" style="display:none">test</div>
                    <!-- <div class="image wow fadeInUp" data-wow-offset="20" data-wow-delay="<?php echo $count_animation; ?>s">
								<img src="<?php //echo get_sub_field('image');
                                            ?>" alt="image" />
							</div> -->
                    <div class="image wow fadeInUp" data-wow-offset="20" data-wow-delay="<?php echo $count_animation; ?>s">

                        <video  muted="" playsinline="" autoplay="" loop data-wf-ignore="true"  width="459" height="430" class="player__video viewer" autoplay muted playsinline>
                            <source src="<?php echo $video_webm; ?>" data-wf-ignore="true" />
                        </video>
                        <!-- <div class="android">
								<video muted="" playsinline="" data-wf-ignore="true" loop width="459" height="430" class="player__video viewer">
									<source src="<?php echo $video_webm; ?>" data-wf-ignore="true" />
								</video>
								</div> -->
                        <!-- <div class="ios">
									<img src="<?php echo get_sub_field('image'); ?>" alt="image" />
								</div> -->

                    </div>
                    <div class="content">
                        <span class="wow fadeInUp" data-wow-offset="10" data-wow-delay="0.2s"><?php echo ($language == "ar") ? get_sub_field('title_ar') : get_sub_field('title'); ?></span>
                        <p class="wow fadeInUp" data-wow-offset="10" data-wow-delay="0.3s">
                            <?php echo ($language == "ar") ? get_sub_field('description_ar') : get_sub_field('description'); ?>
                        </p>
                    </div>
                </div>
            <?php $count += 0.4;
                $count++;
            endwhile; ?>
        </div>
    <?php endif; ?>
    <div class="mobile-content-videos" style="display: none;">
        <?php if (have_rows("videos")) : ?>
            <div class="vidoes-section">
                <?php while (have_rows("videos")) : the_row(); ?>
                    <div class="video" defer>
                        <!-- <video id="player" class="video-js vjs-theme-forest" data-setup="{ratio:1.9}" controls autoplay muted loop>
								<source src="<?php //echo get_sub_field('video'); ?>" type='video/mp4' />
								</video> -->
                        <?php echo get_sub_field('video'); ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <div class="reviews-info-tabs">
            <ul class="tabs">
                <li class="active" id="trigger-info">
                    <span><?php echo ($language == "ar") ? 'معلومات عن المنتج' : 'Product Information'; ?></span>
                </li>
                <li id="trigger-reviews">
                    <span><?php echo ($language == "ar") ? 'تقييمات' : 'Reviews'; ?></span>
                </li>
            </ul>
            <div class="content">
                <div class="tab-content review-section">
                    <div class="product-review-content">
                        <div class="pt-content review-section">
                            <div class="comments-container">
                                <?php
                                $user = wp_get_current_user();
                                if (get_comments_number()) : ?>
                                    <div class="right">
                                        <?php
                                        $five_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '5',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $four_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '4',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $three_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '3',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $two_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '2',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $one_rating_comments = get_comments(array(
                                            'post_id' => $product->get_id(),
                                            'meta_query' => array(
                                                array(
                                                    'key' => 'rating',
                                                    'value' => '1',
                                                    'compare' => '='
                                                )
                                            )
                                        ));
                                        $comments_count = count($five_rating_comments) + count($four_rating_comments) + count($three_rating_comments) + count($two_rating_comments) + count($one_rating_comments);

                                        $current_user = wp_get_current_user();

                                        $rating  = $product->get_average_rating();
                                        $count   = $product->get_rating_count();
                                        //echo wc_get_rating_html( $rating, $count );
                                        $five_rate_number = count($five_rating_comments) / $comments_count * 100;
                                        $four_rate_number = count($four_rating_comments) / $comments_count * 100;
                                        $three_rate_number = count($three_rating_comments) / $comments_count * 100;
                                        $two_rate_number = count($two_rating_comments) / $comments_count * 100;
                                        $one_rate_number = count($one_rating_comments) / $comments_count * 100;
                                        ?>
                                        <div class="reviews-box">
                                            <div class="top">
                                                <h5><?php echo ($language == "") ? 'Product Review' : 'تقييم المنتج' ?></h5>
                                                <h6><?php echo ($language == '') ? get_field('page_title_en') : the_title();  ?>
                                                </h6>
                                            </div>
                                            <div class="bottom">
                                                <div class="right">
                                                    <b><?php echo ($language == '') ? floor($rating) . ' from 5 stars' : floor($rating) . ' من 5 نجوم' ?></b>
                                                    <div class="stars">
                                                        <?php for ($i = 1; $i <= floor($rating); $i++) : ?>
                                                            <span class="active"></span>
                                                        <?php endfor; ?>
                                                        <?php for ($i = floor($rating) + 1; $i <= 5; $i++) : ?>
                                                            <span></span>
                                                        <?php endfor; ?>
                                                    </div>
                                                    <p><?php echo ($language == '') ? get_comments_number() . ' Review' : get_comments_number() . ' تقييم' ?>
                                                    </p>
                                                </div>
                                                <ul>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '5 Stars' : '5 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $five_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($five_rate_number)) ? round($five_rate_number) : $five_rate_number; ?>%</span>
                                                    </li>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '4 Stars' : '4 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $four_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($four_rate_number)) ? round($four_rate_number) : $four_rate_number; ?>%</span>
                                                    </li>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '3 Stars' : '3 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $three_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($three_rate_number)) ? round($three_rate_number) : $three_rate_number; ?>%</span>
                                                    </li>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '2 Stars' : '2 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $two_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($two_rate_number)) ? round($two_rate_number) : $two_rate_number; ?>%</span>
                                                    </li>
                                                    <li>
                                                        <span><?php echo ($language == "") ? '1 Stars' : '1 نجوم'; ?></span>
                                                        <p><b style="width:<?php echo $one_rate_number; ?>%">line</b></p>
                                                        <span><?php echo (is_float($one_rate_number)) ? round($one_rate_number) : $one_rate_number; ?>%</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                        //if($current_user->ID == 0): 
                                        ?>
                                        <!-- <span class="checkout-login"><?php echo ($language == "") ? 'Leave Comment' : 'أكتب تقييم' ?></span> -->
                                        <?php //endif; 
                                        ?>
                                    </div>

                                    <div class="left">
                                        <p class="review-success" style="display: none;">
                                            <?php echo ($language == "") ? 'Thank you, your review is waiting for approval' : 'شكرا لك ، رأيك في انتظار الموافقة'; ?>
                                        </p>
                                        <?php //if($current_user->ID == 0): 
                                        ?>
                                        <!-- <h3>
													<?php echo ($language == '') ? 'In your opinion, how much is the product worth?' : 'في رأيك، كام يستحق المنتج؟'; ?>
												</h3> -->
                                        <?php //endif; 
                                        ?>
                                        <div id="comments" data-user="<?php echo get_current_user_id(); ?>" data-user-name="<?php echo $user->first_name . ' ' . $user->last_name; ?>" data-user-email="<?php echo $user->user_email; ?>">
                                            <?php
                                            if (comments_open() || get_comments_number()) :
                                                comments_template('/comments.php');
                                            endif;
                                            ?>
                                        </div>
                                    </div>
                                <?php else : ?>

                                    <div class="right no-rev">
                                        <div class="reviews-box">
                                            <div class="top">
                                                <h5><?php echo ($language == "") ? 'Product Review' : 'تقييم المنتج' ?></h5>
                                                <h6><?php echo ($language == '') ? get_field('page_title_en') : the_title();  ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="left no-rev">
                                        <p class="review-success" style="display: none;">
                                            <?php echo ($language == "") ? 'Thank you, your review is waiting for approval' : 'شكرا لك ، رأيك في انتظار الموافقة'; ?>
                                        </p>
                                        <div id="comments" data-user="<?php echo get_current_user_id(); ?>" data-user-name="<?php echo $user->first_name . ' ' . $user->last_name; ?>" data-user-email="<?php echo $user->user_email; ?>">
                                            <?php
                                            if (comments_open() || get_comments_number()) :
                                                comments_template('/comments.php');
                                            endif;
                                            ?>
                                        </div>
                                        <?php if (get_comments_number() == 0) : ?>
                                            <p class="noreviews">
                                                <?php echo ($language == "") ? 'There are no reviews on this product yet, you can start writing your first review' : 'لا يوجد تقييمات على هذا المنتج بعد، يمكنك البدء بكتابة أول تقييم' ?>
                                            </p>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-content product-info active">
                    <h4><?php echo ($language == "ar") ? get_field('ar_title') : get_the_title(); ?></h4>
                    <div class="height2">
                        <?php echo ($language == "ar") ? get_field('ar_description') : apply_filters('the_content', get_the_content('', false)); ?>
                    </div>
                    <span id="readMore"><?php echo ($language == 'ar') ? 'اقرا المزيد' : ' Read More' ?> </span>
                    <span id="readLess" style="display:none"><?php echo ($language == 'ar') ? 'اقرا اقل' : 'Read Less' ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

// endif;
/**
 * Hook: woocommerce_after_single_product_summary.
 *
 * @hooked woocommerce_output_product_data_tabs - 10
 * @hooked woocommerce_upsell_display - 15
 * @hooked woocommerce_output_related_products - 20
 */
//do_action( 'woocommerce_after_single_product_summary' );
?>
<!-- </div> -->

<?php //do_action( 'woocommerce_after_single_product' ); 
?>