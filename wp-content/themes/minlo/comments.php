<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
global $language;
if ( post_password_required() ) {
	return;
}
if(is_singular('post')):
	$user = wp_get_current_user();
?>
	<div class="product-review-content">
		<div class="pt-content review-section">
			<div class="comments-container">
<?php endif;?>

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<?php //the_comments_navigation(); ?>
		<?php if(is_singular('post')):?>
			<div class="left left-comments">
				<div class="woocommerce-Reviews">
				<div id="comments" data-user="<?php echo get_current_user_id();?>" data-user-name="<?php echo $user->first_name.' '.$user->last_name;?>" data-user-email="<?php echo $user->user_email;?>">
				<?php endif;?>
		<ol class="comment-list">
			<?php
			if(is_singular('post')):
				wp_list_comments(
					array(
						'callback' => 'blog_comments_list',
						'style'      => 'ol',
						'short_ping' => true,
					)
				);
			else:
			wp_list_comments(
				array(
					'style'      => 'ol',
					'short_ping' => true,
				)
			);
		endif;
			?>
		</ol><!-- .comment-list -->
		<?php if(is_singular('post')):?>
			</div>
			</div>
			</div>
		<?php
		endif;
		//the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'minlo' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().
 ?>
	<?php if(!is_singular('post')):?>
		<?php comment_form(); ?>
	<?php else:?>
			<div class="right no-rev">
				<div class="reviews-box">
					<div class="top">
						<h5><?php echo ($language == "")?'Article Comments':'التعليقات' ?></h5>
						<h6><?php echo ($language == '')? get_field('ar_title') : the_title();  ?></h6>
					</div>
				</div>
			</div>
			<div class="left no-rev">
				<p class="review-success" style="display: none;"><?php echo ($language=="")?'Thank you, your review is waiting for approval':'شكرا لك ، رأيك في انتظار الموافقة';?></p>
				<div id="comments" data-user="<?php echo get_current_user_id();?>" data-user-name="<?php echo $user->first_name.' '.$user->last_name;?>" data-user-email="<?php echo $user->user_email;?>">
					<form action="http://139.162.130.202:81/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
					
						<?php if(!is_user_logged_in(  )):?>
						<p class="comment-form-author">
							<label for="author">
							<?php echo($language=="")?'Name':'الاسم';?>
							<span class="required">
								*
							</span>
							</label>
							
							<input id="author" name="author" type="text" value="" size="30" aria-required="true">
						</p>
						<p class="comment-form-email">
							<label for="email">
							<?php echo($language=="")?'Email':'البريد الإلكتروني';?>
							<span class="required">
								*
							</span>
							</label>
							<input id="email" name="email" type="email" value="" size="30" aria-required="true">
						</p>
						<?php endif;?>
						<p class="comment-form-comment">
							<label for="comment">
							<?php echo($language=="")?"Leave a comment":"آترك التقييم";?>
							</label>
							
							<textarea id="comment" name="comment" cols="42" rows="5" aria-required="true"></textarea>
						</p>
						<p class="form-submit">
							<input name="submit" type="submit" id="submit" value="<?php echo($language=="")?"Leave a review":"آترك التقييم";?>">
							<input type="hidden" name="comment_post_ID" value="<?php echo get_the_ID();?>" id="comment_post_ID">
							<input type="hidden" name="comment_parent" id="comment_parent" value="0">
						</p>
					</form>
				</div>
				<?php if(get_comments_number() == 0): ?>
					<p class="noreviews"><?php echo ($language == "")?'There are no reviews on this product yet, you can start writing your first review':'لا يوجد تقييمات على هذا المنتج بعد، يمكنك البدء بكتابة أول تقييم' ?></p>
				<?php endif; ?>
			</div>
</div>
</div>
<div class="spinner" style="display: none;"><div class="double-bounce1"></div><div class="double-bounce2"></div></div>
</div>
	<?php 
    endif;
	?>
<!-- #comments -->
