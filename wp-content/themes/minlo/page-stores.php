<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

global $language;
get_header();
?>
    <!-- Start Page Content -->
    <div class="page-content page-stores">
        <div class="container">
            <div class="stores">
                <div class="title">
                    <h1 class="wow fadeInUp" data-wow-delay=".5s"><?php echo($language=="ar")?get_field('hero_title_ar'):get_field('hero_title');?></h1>
                    <p class="wow fadeInUp" data-wow-delay=".6s"><?php echo($language=="ar")?get_field('hero_subtitle_ar'):get_field('hero_subtitle');?></p>
                </div>
                <?php if(have_rows('minlo_stores')): $count = 0.7;?>
                <div class="widgets-con">
                    <?php while(have_rows('minlo_stores')): the_row();?>
                    <div class="col wow fadeIn" data-wow-delay="<?php echo $count;?>">
                        <img src="<?php echo get_sub_field('image');?>" alt="" width="100%" height="100%">
                    </div>
                    <?php $count+=0.1; endwhile;?>
                </div>
                <?php endif;?>
            </div>
            <div class="dec-sep">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/stores/store-dec.png" alt="" class="wow fadeInUp" data-wow-delay=".5s" width="100%" height="100%">
            </div>
            <div class="stores">
                <div class="title">
                    <h1 class="wow fadeInUp" data-wow-delay=".1s" data-wow-offset="0"><?php echo($language=="ar")?get_field('stores_title_ar'):get_field('stores_title');?></h1>
                    <p class="wow fadeInUp" data-wow-delay=".2s" data-wow-offset="0"><?php echo($language=="ar")?get_field('stores_subtitle_ar'):get_field('stores_subtitle');?></p>
                </div>
                <div class="widgets-con online-store">
                    <?php if(have_rows('online_stores')): $count=0.1; while(have_rows('online_stores')): the_row();?>
                    <div class="col wow fadeIn" data-wow-delay="<?php echo $count;?>" data-wow-offset="0">
                        <span><?php echo($language=="ar")?get_sub_field('title_ar'):get_sub_field('title');?></span>
                        <img src="<?php echo get_sub_field('image');?>  "width="100%" height="100%" alt="">
                    </div>
                    <?php $count+=0.1; endwhile; endif;?>
                </div>
            </div>
            <div class="dec-sep">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/stores/store-dec.png" alt="" width="100%" height="100%">
            </div>
            <div class="stores">
                <div class="title">
                    <h1 class="wow fadeInUp" data-wow-delay=".1s" data-wow-offset="0"><?php echo($language=="ar")?get_field('offline_stores_title_ar'):get_field('offline_stores_title');?></h1>
                    <p class="wow fadeInUp" data-wow-delay=".2s" data-wow-offset="0"><?php echo($language=="ar")?get_field('offline_stores_subtitle_ar'):get_field('our_stores_subtitle');?></p>
                </div>
                <ul class="logos-con">
                <?php if(have_rows('offline_stores')): $count=0.1; while(have_rows('offline_stores')): the_row();?>
                    <li class="logo-col wow fadeIn" data-wow-delay="<?php echo $count;?>s" data-wow-offset="0">
                        <div class="image">
                            <img src="<?php echo get_sub_field('image');?>" alt="" width="100%" height="100%">
                        </div>
                    </li>
                <?php $count+=0.1; endwhile; endif;?>
                </ul>
            </div>
            <div class="stores-map wow fadeIn" data-wow-offset="0">
                <img src="<?php echo get_field('map_image');?>" alt="" width="100%" height="100%"> 
            </div>
        </div>
    </div>
<?php
get_footer();
