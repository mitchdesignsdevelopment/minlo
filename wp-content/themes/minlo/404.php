<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package minlo
 */

get_header();
?>

	<main id="primary" class="site-main 404-page">

		<section class="error-404 not-found">
			<header class="page-header">
				<h1 class="page-title">
				<?php echo($language == 'ar')? '404' : '404'; ?>
				</h1>
			</header><!-- .page-header -->

			<div class="page-content">
				<p>	<?php echo($language == 'ar')? 'الصفحة غير موجودة':' Page Not Found'; ?></p>


					<div class="widget widget_categories">
						<h2 class="widget-title">
						<?php echo($language == 'ar')? 'الصفحة غير موجودة':' Page Not Found'; ?>		
					</h2>
						<a href="<?php bloginfo('url'); echo($language == 'ar')? '?lang=ar' : ''; ?>"><?php echo($language == 'ar')? 'الصفحة الرئيسية':'Go to Homepage'; ?></a>
					</div><!-- .widget -->

					

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
