if ($('body').hasClass('rtl')) {
  var is_ar = true;
} else {
  var is_ar = false;
}

// Animation
document.addEventListener("DOMContentLoaded", () => {
  let controller = new ScrollMagic.Controller();
}); //END OF Animation
$(".col-sm-12.single").append('<div class="g-recaptcha" data-sitekey="6LcCHUgkAAAAAKK_pq3AhgKeogyL772R215igfh0"></div>')

//Main Actions
$(document).ready(function () {

  // //Make Page Fit
  var HeaderHeight = $(".page-header").innerHeight();
  var HeaderFooter = $(".page-footer").innerHeight();
  var theHeight = $(window).innerHeight() - HeaderHeight - HeaderFooter;
  $(".page-content").css({ "min-height": theHeight });

  $(window).scroll(function () {
    var sticky = $(".sticky"),
      scroll = $(window).scrollTop();
    if ($('.site-main').hasClass('404-page')) {
      console.log('404 Page')
    } else {
      if (scroll >= 100)
        sticky.addClass("fixed"),
          $("body").addClass("sticky-header"),
          $(".dgwt-wcas-suggestions-wrapp").addClass("scrolled"),
          $(".filter-tab").addClass("top");
      else
        sticky.removeClass("fixed"),
          $("body").removeClass("sticky-header"),
          $(".dgwt-wcas-suggestions-wrapp").removeClass("scrolled"),
          $(".filter-tab").removeClass("top");
    }

  });

  $(".trigger-menu").on("click", function () {
    $("body").addClass("no-scroll");
    $(".header-content").addClass("active");
  });

  $(".close-menu").on("click", function () {
    $("body").removeClass("no-scroll");
    $(".header-content").removeClass("active");
    $(".close-mega").removeClass("active");
  });

  $(".page-header").on("click", function () {
    $(".close-mega").removeClass("active");
  });

  $(document).on("click", ".has-menu", function () {
    $(this).find(".mega-menu").addClass("active");
    $(".close-mega").addClass("active");
  });

  $(document).on("click", ".close-mega", function () {
    $(this).removeClass("active");
    $(this).find(".mega-menu").removeClass("active");
  });

  $(".hero-banner").slick({
    rtl: is_ar,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    dots: true,
    infinite: false,
    autoplay: false,
    fade: true,
    speed: 800,
    autoplaySpeed: 2000,
    nextArrow:
      '<div class="next-arrow"><span><i class="material-icons">chevron_right</i></span></div>',
    prevArrow:
      '<div class="prev-arrow"><span><i class="material-icons">chevron_left</i></span></div>',
    responsive: [
      {
        breakpoint: 999,
        infinite: false,
        fade: true,
      },
    ],
  });

  $(window).bind("load", function () {
    setTimeout(() => {
      $(".hero-banner .slick-arrow").addClass("active");
      $(".hero-banner .slick-dots").addClass("active");
    }, 8500);
  });

  $(".product-banner-gallery").slick({
    rtl: is_ar,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    dots: true,
    infinite: false,
    autoplay: false,
    fade: true,
    speed: 800,
    autoplaySpeed: 2000,
    nextArrow:
      '<div class="next-arrow"><span><i class="material-icons">chevron_right</i></span></div>',
    prevArrow:
      '<div class="prev-arrow"><span><i class="material-icons">chevron_left</i></span></div>',
    responsive: [
      {
        breakpoint: 999,
        infinite: false,
        fade: true,
      },
    ],
  });

  $(window).on("load", function () {
    $(".product-banner-gallery").addClass('active');
  });

  $(window).bind("load", function () {
    setTimeout(() => {
      $(".product-banner-gallery .slick-arrow").addClass("active");
      $(".product-banner-gallery .slick-dots").addClass("active");
    }, 7000);
  });

  $(".product-slider").slick({
    rtl: is_ar,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    dots: true,
    asNavFor: ".slider-nav",
    autoplay: false,
    nextArrow: '<div class="next-arrow"><span></span></div>',
    prevArrow: '<div class="prev-arrow"><span></span></div>',
  });

  $(".slider-nav").slick({
    rtl: is_ar,
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: ".product-slider",
    dots: false,
    arrows: true,
    focusOnSelect: true,
    infinite: false,
    autoplay: false,
    vertical: true,
    verticalSwiping: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          vertical: false,
          verticalSwiping: false,
        },
      },
    ]
  });

  // Start Make About Tabs Collapes
  $(document).on("click", ".tab:not(.active)", function () {
    $(this).addClass("active").siblings().removeClass("active");
    $(this).find(".open-ques").hide();
    $(this).find(".hide-ques").show();
    $(this).find(".content").slideDown();
    $(this)
      .find(".content")
      .slideDown()
      .closest(".tab")
      .siblings()
      .find(".content")
      .slideUp()
      .parent()
      .find(".open-ques")
      .show()
      .closest(".tab")
      .find(".hide-ques")
      .hide();
  });

  $(document).on("click", ".tab.active", function () {
    $(this).removeClass("active");
    $(this).find(".open-ques").show();
    $(this).find(".hide-ques").hide();
    $(this).find(".content").slideUp();
  });

  $(document).on("click", ".open-ques", function () {
    $(this).hide();
    $(this).parent().addClass("active").siblings().removeClass("active");
    $(this).siblings(".content").slideDown();
    $(this).siblings(".hide-ques").show();
  });

  $(document).on("click", ".hide-ques", function () {
    $(this).hide();
    $(this).parent().removeClass("active");
    $(this).siblings(".content").slideUp();
    $(this).siblings(".open-ques").show();
  });
  // End Make Tabs Collapes

  jQuery(document).on("click", ".view-timeline", function () {
    $(this).addClass("open");
    $(this).addClass("active");
    $(".story-tabs").removeClass("expand");
    $(".story-tabs").addClass("active");
    $(".close-timeline").show();
    $(".open-timeline").hide();
  });

  jQuery(document).on("click", ".view-timeline.open.active", function () {
    $(this).removeClass("active");
    $(".story-tabs").addClass("expand");
    $(".story-tabs").removeClass("active");
    $(".close-timeline").hide();
    $(".open-timeline").show();
  });


  $(".filter li").click(function () {
    var valNEw = $(this).data("value");
    $(this).addClass("active").siblings().removeClass("active");
    var $el = $("." + valNEw).addClass("show");
    $(".tab").not($el).removeClass("show");
  });

  $(".filter li.all").click(function () {
    $(".tab").addClass("show");
  });

  $(".reviews-info-tabs .tabs li").click(function () {
    $(this).addClass("active").siblings().removeClass("active");
  });

  $(".reviews-info-tabs .tabs li#trigger-reviews").click(function () {
    $(".review-section").addClass("active");
    $(".product-info").removeClass("active");
  });

  $(".reviews-info-tabs .tabs li#trigger-info").click(function () {
    $(".product-info").addClass("active");
    $(".review-section").removeClass("active");
  });

  $(".moveTop").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 800);
    return false;
  });

  $(function () {
    if (".single-product-con .right") {
      $(".single-product-con .right").StickySidebar({
        // Settings
        additionalMarginTop: 100,
      });
    }
  });

  (function ($) {
    $.fn.StickySidebar = function (options) {
      var defaults = {
        containerSelector: "",
        additionalMarginTop: 0,
        additionalMarginBottom: 0,
        updateSidebarHeight: true,
        minWidth: 0,
        disableOnResponsiveLayouts: true,
        sidebarBehavior: "modern",
      };
      options = $.extend(defaults, options);

      // Validate options
      options.additionalMarginTop = parseInt(options.additionalMarginTop) || 0;
      options.additionalMarginBottom =
        parseInt(options.additionalMarginBottom) || 0;

      tryInitOrHookIntoEvents(options, this);

      // Try doing init, otherwise hook into window.resize and document.scroll and try again then.
      function tryInitOrHookIntoEvents(options, $that) {
        var success = tryInit(options, $that);

        if (!success) {
          console.log(
            "TST: Body width smaller than options.minWidth. Init is delayed."
          );

          $(document).scroll(
            (function (options, $that) {
              return function (evt) {
                var success = tryInit(options, $that);

                if (success) {
                  $(this).unbind(evt);
                }
              };
            })(options, $that)
          );
          $(window).resize(
            (function (options, $that) {
              return function (evt) {
                var success = tryInit(options, $that);

                if (success) {
                  $(this).unbind(evt);
                }
              };
            })(options, $that)
          );
        }
      }

      // Try doing init if proper conditions are met.
      function tryInit(options, $that) {
        if (options.initialized === true) {
          return true;
        }

        if ($("body").width() < options.minWidth) {
          return false;
        }

        init(options, $that);

        return true;
      }

      // Init the sticky sidebar(s).
      function init(options, $that) {
        options.initialized = true;

        // Add CSS
        $("head").append(
          $(
            '<style>.StickySidebar:after {content: ""; display: table; clear: both;}</style>'
          )
        );

        $that.each(function () {
          var o = {};

          o.sidebar = $(this);

          // Save options
          o.options = options || {};

          // Get container
          o.container = $(o.options.containerSelector);
          if (o.container.length == 0) {
            o.container = o.sidebar.parent();
          }

          // Create sticky sidebar
          o.sidebar.parents().css("-webkit-transform", "none"); // Fix for WebKit bug - https://code.google.com/p/chromium/issues/detail?id=20574
          o.sidebar.css({
            position: "relative",
            overflow: "visible",
            // The "box-sizing" must be set to "content-box" because we set a fixed height to this element when the sticky sidebar has a fixed position.
            "-webkit-box-sizing": "border-box",
            "-moz-box-sizing": "border-box",
            "box-sizing": "border-box",
          });

          // Get the sticky sidebar element. If none has been found, then create one.
          o.stickySidebar = o.sidebar.find(".StickySidebar");
          if (o.stickySidebar.length == 0) {
            o.sidebar.find("script").remove();
            o.stickySidebar = $("<div>")
              .addClass("StickySidebar")
              .append(o.sidebar.children());
            o.sidebar.append(o.stickySidebar);
          }

          // Get existing top and bottom margins and paddings
          o.marginTop = parseInt(o.sidebar.css("margin-top"));
          o.marginBottom = parseInt(o.sidebar.css("margin-bottom"));
          o.paddingTop = parseInt(o.sidebar.css("padding-top"));
          o.paddingBottom = parseInt(o.sidebar.css("padding-bottom"));

          // Add a temporary padding rule to check for collapsable margins.
          var collapsedTopHeight = o.stickySidebar.offset().top;
          var collapsedBottomHeight = o.stickySidebar.outerHeight();
          o.stickySidebar.css("padding-top", 1);
          o.stickySidebar.css("padding-bottom", 1);
          collapsedTopHeight -= o.stickySidebar.offset().top;
          collapsedBottomHeight =
            o.stickySidebar.outerHeight() -
            collapsedBottomHeight -
            collapsedTopHeight;
          if (collapsedTopHeight == 0) {
            o.stickySidebar.css("padding-top", 0);
            o.stickySidebarPaddingTop = 0;
          } else {
            o.stickySidebarPaddingTop = 1;
          }

          if (collapsedBottomHeight == 0) {
            o.stickySidebar.css("padding-bottom", 0);
            o.stickySidebarPaddingBottom = 0;
          } else {
            o.stickySidebarPaddingBottom = 1;
          }

          // We use this to know whether the user is scrolling up or down.
          o.previousScrollTop = null;

          // Scroll top (value) when the sidebar has fixed position.
          o.fixedScrollTop = 0;

          // Set sidebar to default values.
          resetSidebar();

          o.onScroll = function (o) {
            // Stop if the sidebar isn't visible.
            if (!o.stickySidebar.is(":visible")) {
              return;
            }

            // Stop if the window is too small.
            if ($("body").width() < o.options.minWidth) {
              resetSidebar();
              return;
            }

            // Stop if the sidebar width is larger than the container width (e.g. the theme is responsive and the sidebar is now below the content)
            if (o.options.disableOnResponsiveLayouts) {
              var sidebarWidth = o.sidebar.outerWidth(
                o.sidebar.css("float") == "none"
              );

              if (sidebarWidth + 50 > o.container.width()) {
                resetSidebar();
                return;
              }
            }

            var scrollTop = $(document).scrollTop();
            var position = "static";

            // If the user has scrolled down enough for the sidebar to be clipped at the top, then we can consider changing its position.
            if (
              scrollTop >=
              o.container.offset().top +
              (o.paddingTop + o.marginTop - o.options.additionalMarginTop)
            ) {
              // The top and bottom offsets, used in various calculations.
              var offsetTop =
                o.paddingTop + o.marginTop + options.additionalMarginTop;
              var offsetBottom =
                o.paddingBottom +
                o.marginBottom +
                options.additionalMarginBottom;

              // All top and bottom positions are relative to the window, not to the parent elemnts.
              var containerTop = o.container.offset().top;
              var containerBottom =
                o.container.offset().top + getClearedHeight(o.container);

              // The top and bottom offsets relative to the window screen top (zero) and bottom (window height).
              var windowOffsetTop = 0 + options.additionalMarginTop;
              var windowOffsetBottom;

              var sidebarSmallerThanWindow =
                o.stickySidebar.outerHeight() + offsetTop + offsetBottom <
                $(window).height();
              if (sidebarSmallerThanWindow) {
                windowOffsetBottom =
                  windowOffsetTop + o.stickySidebar.outerHeight();
              } else {
                windowOffsetBottom =
                  $(window).height() -
                  o.marginBottom -
                  o.paddingBottom -
                  options.additionalMarginBottom;
              }

              var staticLimitTop =
                containerTop - scrollTop + o.paddingTop + o.marginTop;
              var staticLimitBottom =
                containerBottom - scrollTop - o.paddingBottom - o.marginBottom;

              var top = o.stickySidebar.offset().top - scrollTop;
              var scrollTopDiff = o.previousScrollTop - scrollTop;

              // If the sidebar position is fixed, then it won't move up or down by itself. So, we manually adjust the top coordinate.
              if (o.stickySidebar.css("position") == "fixed") {
                if (o.options.sidebarBehavior == "modern") {
                  top += scrollTopDiff;
                }
              }

              if (o.options.sidebarBehavior == "stick-to-top") {
                top = options.additionalMarginTop;
              }

              if (o.options.sidebarBehavior == "stick-to-bottom") {
                top = windowOffsetBottom - o.stickySidebar.outerHeight();
              }

              if (scrollTopDiff > 0) {
                // If the user is scrolling up.
                top = Math.min(top, windowOffsetTop);
              } else {
                // If the user is scrolling down.
                top = Math.max(
                  top,
                  windowOffsetBottom - o.stickySidebar.outerHeight()
                );
              }

              top = Math.max(top, staticLimitTop);

              top = Math.min(
                top,
                staticLimitBottom - o.stickySidebar.outerHeight()
              );

              // If the sidebar is the same height as the container, we won't use fixed positioning.
              var sidebarSameHeightAsContainer =
                o.container.height() == o.stickySidebar.outerHeight();

              if (!sidebarSameHeightAsContainer && top == windowOffsetTop) {
                position = "fixed";
              } else if (
                !sidebarSameHeightAsContainer &&
                top == windowOffsetBottom - o.stickySidebar.outerHeight()
              ) {
                position = "fixed";
              } else if (
                scrollTop + top - o.sidebar.offset().top - o.paddingTop <=
                options.additionalMarginTop
              ) {
                // Stuck to the top of the page. No special behavior.
                position = "static";
              } else {
                // Stuck to the bottom of the page.
                position = "absolute";
              }
            }

            /*
             * Performance notice: It's OK to set these CSS values at each resize/scroll, even if they don't change.
             * It's way slower to first check if the values have changed.
             */
            if (position == "fixed") {
              o.stickySidebar.css({
                position: "fixed",
                width: o.sidebar.width(),
                top: top,
                left:
                  o.sidebar.offset().left +
                  parseInt(o.sidebar.css("padding-left")),
              });
            } else if (position == "absolute") {
              var css = {};

              if (o.stickySidebar.css("position") != "absolute") {
                css.position = "absolute";
                css.top =
                  scrollTop +
                  top -
                  o.sidebar.offset().top -
                  o.stickySidebarPaddingTop -
                  o.stickySidebarPaddingBottom;
              }

              css.width = o.sidebar.width();
              css.left = "";

              o.stickySidebar.css(css);
            } else if (position == "static") {
              resetSidebar();
            }

            if (position != "static") {
              if (o.options.updateSidebarHeight == true) {
                o.sidebar.css({
                  "min-height":
                    o.stickySidebar.outerHeight() +
                    o.stickySidebar.offset().top -
                    o.sidebar.offset().top +
                    o.paddingBottom,
                });
              }
            }

            o.previousScrollTop = scrollTop;
          };

          // Initialize the sidebar's position.
          o.onScroll(o);
      // Recalculate the sidebar's position on every scroll and resize.
          $(document).scroll(
            (function (o) {
              return function () {
                o.onScroll(o);
              };
            })(o)
          );
          $(window).resize(
            (function (o) {
              return function () {
                o.stickySidebar.css({ position: "static" });
                o.onScroll(o);
              };
            })(o)
          );

          // Reset the sidebar to its default state
          function resetSidebar() {
            o.fixedScrollTop = 0;
            o.sidebar.css({
              "min-height": "1px",
            });
            o.stickySidebar.css({
              position: "static",
              width: "",
            });
          }

          // Get the height of a div as if its floated children were cleared. Note that this function fails if the floats are more than one level deep.
          function getClearedHeight(e) {
            var height = e.height();

            e.children().each(function () {
              height = Math.max(height, $(this).height());
            });

            return height;
          }
        });
      }
    };
  })(jQuery);


  $(".brand-slider").slick({
    rtl: is_ar,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    dots: true,
    infinite: false,
    autoplay: false,
    fade: true,
    speed: 800,
    autoplaySpeed: 2000,
    nextArrow:
      '<div class="next-arrow"><span><i class="material-icons">chevron_right</i></span></div>',
    prevArrow:
      '<div class="prev-arrow"><span><i class="material-icons">chevron_left</i></span></div>',
    responsive: [
      {
        breakpoint: 999,
        infinite: false,
        fade: true,
      },
    ],
  });
});
// here

jQuery(document).on("change", ".filter_input", function () {
  if (!$(".products").hasClass("list-products")) {
    $(".products").addClass("list-products");
  }
  $posts_per_page = 20;
  $(".products").data("page", 1);
  get_products_ajax("filter", "desktop");
});

jQuery(window).scroll(function () {
  if ($(".spinner").is(":visible")) {
    if ($(".product-widget").length) {
      Footeroffset = jQuery(".product-widget").last().offset().top;
    }
    winPosBtn = jQuery(window).scrollTop();
    winH = jQuery(window).outerHeight();
    if (winPosBtn + winH > Footeroffset + 5) {
      get_products_ajax("loadmore");
    }
  }
});
// load more on scroll and click, filter, and sort
$posts_per_page = 20;
$loading_more = false;

function get_products_ajax(action, view = "") {
  console.log("called get_products_ajax");
  var ajax_url = $("meta[name=ajax_url]").attr("content");
  $count = $(".products").attr("data-count");
  $page = $(".products").attr("data-page");
  $posts = $(".products").attr("data-posts");
  $type = $(".products").attr("data-type");
  $search = $(".products").attr("data-search");
  $lang = $(".products-list").attr("data-lang");
  $slug = "";
  if ($type == "shop") {
  } else {
    $slug = $(".products").data("slug");
    $cat = $(".products").data("cat");
  }

  let brand = new Array();

  $(".filter_input:checked").each(function () {
    if ($(this).hasClass("filter-brand")) {
      brand.push($(this).val());
    }
  });
  if (($loading_more || $posts_per_page >= $posts) && action == "loadmore") {
    console.log("khalstt " + $posts);
    return;
  }
  $loading_more = true;
  $.ajax({
    type: "POST",
    url: ajax_url,
    data: {
      action: "get_products_ajax",
      count: $count,
      page: $page,
      //   order: $order,
      type: $type,
      slug: $slug,
      brand: brand,
      search: $search,
      fn_action: action,
      lang: $lang,
    },
    success: function (posts) {
      
      get_products_ajax_count(action);
      $loading_more = false;
      $(".products").removeClass("list-products");
      $(".filter-loader").removeClass("active");
      if (action == "loadmore") {
        $(".products-list .links").append(posts);
        $(".products").attr("data-page", parseInt($page) + 1);
        $(".load-more-products").attr("data-page", parseInt($page) + 1);
        $(".spinner").attr("data-page", parseInt($page) + 1);
        $(".products").data("page", parseInt($page) + 1);
        $(".load-more-products").data("page", parseInt($page) + 1);
        $(".spinner").data("page", parseInt($page) + 1);
        console.log($(".products").attr("data-page"));
        $posts_per_page += parseInt($count);
        $posts = $(".products").attr("data-posts");
        console.log("$posts_per_page", $posts_per_page);
        console.log("$posts", $posts);
        if ($posts_per_page >= $posts) {
          /// Begin of get out of stock products function
          $(".spinner").hide();
        } else {
          if ($posts_per_page < $posts) {
            $(".spinner").show();
          }
        }
      } else {
        $(".products").html(posts);
        if (parseInt($page) % 2 == 0 && $posts_per_page < $posts) {
          $(".spinner").show();
        } else if (parseInt($page) % 2 == 1 && $posts_per_page < $posts) {
          $(".spinner").show();
        } else if ($posts_per_page >= $posts) {
          /// Begin of get out of stock products function
          $(".spinner").hide();
        }
      }
    },
  });
}

function get_products_ajax_count(view) {
  var ajax_url = $("meta[name=ajax_url]").attr("content");
  $count = $(".products").attr("data-count");
  $page = $(".products").attr("data-page");
  $posts = $(".products").attr("data-posts");
  $order = $(".products").attr("data-sort");
  $type = $(".products").attr("data-type");
  $search = $(".products").attr("data-search");
  $slug = "";
  if ($type == "shop") {
  } else {
    $slug = $(".products").data("slug");
    $cat = $(".products").data("cat");
  }
  let brand = new Array();
  let cats = new Array();

  $(".filter_input:checked").each(function () {
    if ($(this).hasClass("filter-brand")) {
      brand.push($(this).val());
    }
  });

  setTimeout(function () {
    $.ajax({
      type: "POST",
      url: ajax_url,
      data: {
        action: "get_products_ajax_count",
        count: $count,
        page: $page,
        order: $order,
        type: $type,
        slug: $slug,
        brand: brand,
        search: $search,
      },
      success: function (posts) {
        $(".filter-loader").removeClass("active");
        if (20 >= parseInt(posts)) {
          $(".spinner").addClass("hide");
        } else if (parseInt(posts) == 0) {
          $(".spinner").removeClass("hide");
        } else {
          $(".spinner").removeClass("hide");
        }
        $(".products").attr("data-posts", posts);
        $(".spinner").attr("data-posts", posts);
      },
    });
  });
}

$(window).bind("load", function () {
  if ($("#commentform").length) {
    let urlParams = new URLSearchParams(window.location.search);
    let lang = "";
    if (urlParams.has("lang")) {
      lang = urlParams.get("lang");
    }
    $("#commentform").append(
      '<input type="hidden" id="redirect_to" name="redirect_to" value="' +
      window.location.href +
      '">'
    );
    if (window.location.href.indexOf("product") > -1) {
    } else {
      $(".comment-form-attachment").remove();
    }
    if ($(".comment-form-attachment").length) {
      var wrapper = $("<div/>").css({
        height: 0,
        width: 0,
        overflow: "hidden",
      });
      var fileInput = $(":file").wrap(wrapper);
      if (lang == "") {
        $(".comment-form-attachment__label").text("Upload Image");
      } else {
        $(".comment-form-attachment__label").text("اضف صورة");
      }
      fileInput.change(function () {
        $this = $(this);
        $(".comment-form-attachment__label p").remove();
        if (lang == "") {
          if ($this[0].files.length == 1) {
            $(".comment-form-attachment__label")
              .append("<p>" + $this[0].files.length + " file</p>")
              .siblings("p")
              .remove();
          } else {
            $(".comment-form-attachment__label")
              .append("<p>" + $this[0].files.length + " files</p>")
              .siblings("p")
              .remove();
          }
        } else {
          if ($this[0].files.length == 1) {
            $(".comment-form-attachment__label")
              .append("<p>" + $this[0].files.length + " ملف</p>")
              .siblings("p")
              .remove();
          } else {
            $(".comment-form-attachment__label")
              .append("<p>" + $this[0].files.length + " ملفات</p>")
              .siblings("p")
              .remove();
          }
        }
      });
    }
    $("#commentform").submit(function (e) {
      var author = "";
      var email = "";
      if ($("#commentform #author").length) {
        author = $("#commentform #author").val();
      } else {
        author = "notFound";
      }
      if ($("#commentform #email").length) {
        email = $("#commentform #email").val();
      } else {
        email = "notFound";
      }
      if (!author) {
        $("#commentform #author").addClass("validate");
      } else {
        $("#commentform #author").removeClass("validate");
      }
      if (!email) {
        $("#commentform #email").addClass("validate");
      } else {
        $("#commentform #email").removeClass("validate");
      }
      if (!$("#commentform #comment").val()) {
        $("#commentform #comment").addClass("validate");
      } else {
        $("#commentform #comment").removeClass("validate");
      }
      if (author && email && $("#commentform #comment").val()) {
        $(".review-success").hide();
        $(".spinner").show();
        var ajax_url = $("meta[name=ajax_url]").attr("content");
        var customer_name = $("#author").val();
        var email = $("#email").val();
        var rating = $("#rating").val();
        var sku = $(".product-sku").text();
        var review = $("#comment").val();
        var post_id = $("#comment_post_ID").val();
        var user_id = $("#comments").attr("data-user");
        if (user_id != 0) {
          email = $("#comments").attr("data-user-email");
          customer_name = $("#comments").attr("data-user-name");
        }
        var form = new FormData();
        if ($("#attachment").length) {
          var image = $("#attachment")[0].files;
          jQuery.each(jQuery("#attachment")[0].files, function (i, file) {
            form.append("file[]", file);
          });
          form.append("image", image);
        }
        form.append("action", "handle_my_file_upload");
        if (customer_name) {
          form.append("customer_name", customer_name);
        }
        if (rating) {
          form.append("rating", rating);
        }
        if (sku) {
          form.append("sku", sku);
        }
        form.append("review", review);
        form.append("email", email);
        if (user_id) {
          form.append("user_id", user_id);
        }
        if (post_id) {
          form.append("post_id", post_id);
        }
        // console.log(image);
        setTimeout(function () {
          $.ajax({
            type: "POST",
            url: ajax_url,
            data: form,
            processData: false, //Very important
            contentType: false, //Very important
            success: function (posts) {
              $(".spinner").hide();
              $(".star-" + $("#rating").val()).removeClass("active");
              $("#commentform")[0].reset();
              $(".stars").removeClass("selected");
              $(".comment-form-attachment__label p").remove();
              $("#rating").prop("selectedIndex", 0);
              $(".review-success").show();
            },
          });
        }, 1000);
      }
      e.preventDefault();
    });
  }
});
$(".dco-image-attachment").on("click", function () {
  $(this)
    .parent()
    .parent()
    .parent()
    .find(".comment-attachments")
    .addClass("active");
  $("#reviews-overlay").addClass("overlay_visible");
});

$(".close-attachment").on("click", function () {
  $(".comment-attachments").removeClass("active");
  $("#reviews-overlay").removeClass("overlay_visible");
});


$("#reviews-overlay").on("click", function () {
  $(".comment-attachments").removeClass("active");
  $(this).removeClass("overlay_visible");
});

if ($('.hero-banner .image').length) {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $(".hero-banner .image img").each(function () {
      $(this).attr('src', $(this).attr('data-mobile-src'));
    });
  }
}
if ($('.contact-content .image').length) {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $(".contact-content .image img").each(function () {
      $(this).attr('src', $(this).attr('data-mobile-src'));
    });
  }
}
if ($('.about-intro .image').length) {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $(".about-intro .image img").each(function () {
      $(this).attr('src', $(this).attr('data-mobile-src'));
    });
  }
}

if ($('.careers-container').length) {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $(".careers-container img").each(function () {
      $(this).attr('src', $(this).attr('data-mobile-src'));
    });
  }
}

if ($('.single-brand-banner').length) {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('.single-brand-banner').attr('style', $('.single-brand-banner').attr('data-mobile-style'));
    $(".brand-slider .item").each(function () {
      $(this).attr('style', $(this).attr('data-mobile-style'));
    });
  }
}

if ($('.center-products').length) {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('.center-products.desktop').removeClass('active');
    $('.center-products.mobile').addClass('active');
    $('.mobile-content-videos').show();
    $('.desktop-content-videos').hide();
  }
  else {
    $('.center-products.mobile').removeClass('active');
    $('.center-products.desktop').addClass('active');
    $('.mobile-content-videos').hide();
    $('.desktop-content-videos').show();
    $('.scroll-top.mobile').hide();
  }
}

if ($('.pdf-widget').length) {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('.desktop-pdf').hide();
    $('.mobile-pdf').show();
  }
  else {
    $('.mobile-pdf').hide();
    $('.desktop-pdf').show();
  }
}

let box = document.querySelector('.tab-content.product-info .height2');
if (box) {
  console.log('in ims');
  let height = box.offsetHeight;
  console.log({ height });
  $("#readMore").hide();
  if (height > 112) {
    $("#readMore").show();
  }
  $(document).on('click', "#readMore", function () {
    $(".reviews-info-tabs .product-info .height2").addClass("active");
    $(this).hide();
    $(this).parent().find("#readLess").show();
    $("#readLess").show();
    return false;
  });
}

$(document).on('click', "#readLess", function () {
  $(".reviews-info-tabs .product-info .height2").removeClass("active");
  $(this).hide();
  $(this).parent().find("#readMore").show();
  $("#readMore").show();
  return false;
});

$('.moveTop').on("click", function () {
  $(window).scrollTop(0);
});

// $("#CF61e9aa944be23_1").submit(function(e){
//     var recaptcha_response = $("#g-recaptcha-response").val();
//     if ($(".caldera-grid #g-recaptcha-response").length && !$(".caldera-grid #g-recaptcha-response").val()) {
//       if ($('body').hasClass('rtl')) {
//         alert('من فضلك اختر "أنا لست روبوت"');
//         e.preventDefault()
//       } else {
//         alert('Please select "I\'m not a robot!"');
//         e.preventDefault()

//       }
//       return false;
//     }
//     if ($(".caldera-grid #g-recaptcha-response").length) {
//       form.append("#g-recaptcha-response", recaptcha_response);
//     }
//   })
      

