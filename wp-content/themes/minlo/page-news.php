<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

global $language;
get_header();
?>
    <!-- Start Page Content -->
    <div class="page-content page-news">
        <div class="news-container">
            <div class="title">
                <h1><?php echo($language=="ar")? get_field('ar_title'):get_the_title();?></h1>
                <p><?php echo($language=="ar")? get_field('ar_description'):get_the_content();?></p>
            </div>
            <?php  
             $the_query = new WP_Query( ['post_type'=>'post','posts_per_page' => -1,'post_status'=>'published']);
                 if ( $the_query->have_posts() ) { 
            ?>
            <div class="news-list">
              <?php  while ( $the_query->have_posts() ) {
                 $the_query->the_post();
                 $po_img_src = wp_get_attachment_image_src(get_post_thumbnail_id($the_query->ID), 'full' );
                 $po_img = $po_img_src[0];
                 ?>
                <div class="item">
                    <a href="<?php echo get_the_permalink(); echo($language=="ar")?'?lang=ar':'';?>" class="image">
                        <img src="<?php echo $po_img;?>" alt="image" width="100%" height="100%">
                    </a>
                    <a href="<?php echo get_the_permalink(); echo($language=="ar")?'?lang=ar':'';?>" class="news-title"><?php echo get_the_title();?></a>
                    <span class="date"><?php echo get_the_date('F jS, Y');?></span>
                </div>
                <?php } ?>
                <div class="item">
                    <a href="" class="image">
                        <img src="assets/images/news-img.png" alt="image" width="100%" height="100%">
                    </a>
                    <a href="" class="news-title">Banking on a private sector pickup</a>
                    <span class="date">August 19th, 2019</span>
                </div>
                <div class="item">
                    <a href="" class="image">
                        <img src="assets/images/news-img.png" alt="image" width="100%" height="100%">
                    </a>
                    <a href="" class="news-title">Banking on a private sector pickup</a>
                    <span class="date">August 19th, 2019</span>
                </div>
            </div>
            <?php wp_reset_postdata(); ?>
        <?php } ?>
           
        </div>
    </div>
<?php
get_footer();
