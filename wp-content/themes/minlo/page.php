<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

global $language;
get_header();
?>
<div class="texted-page">
  <div class="grid">
    <div class="page-title">
		<?php if( get_field('title') ): ?>
			<h1><?php $ar_title_field = get_field('page_title_ar');
            echo ($language && $ar_title_field)? $ar_title_field : get_field('title'); ?></h1>
		<?php else:?>
			<h1><?php $ar_title_field = get_field('page_title_ar');
            echo ($language && $ar_title_field)? $ar_title_field : the_title(); ?></h1>
		<?php endif; ?>
		<?php if( get_field('sub_title') ): ?>
          <h5><?php $ar_sub_title_field = get_field('sub_title_ar');
            echo ($language && $ar_sub_title_field)? $ar_sub_title_field : get_field('sub_title'); ?></h5>
        <?php endif; ?>
    </div>
    <div class="wysiwyg">
	  <?php
	  $ar_content_field = get_field('page_content_ar');
	  echo ($language && $ar_content_field)? $ar_content_field : the_content();?>
    </div>
  </div>
</div>
<?php
get_footer();
