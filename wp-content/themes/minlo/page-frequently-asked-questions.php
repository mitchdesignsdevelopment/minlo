<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

global $language;
get_header();
?>
  <!-- Start Page Content -->
  <div class="page-content page-faq">
        <div class="faq-container">
            <div class="page-title">
                <span><img src="<?php echo get_template_directory_uri();?>/assets/images/slogan.png" alt=""  width="100%" height="100%"></span>
                <h1><?php echo($language=="ar")?get_field('ar_title'):get_the_title();?></h1>
                <p><?php echo($language=="ar")?get_field('ar_description'):get_the_content();?></p>
            </div>

            <div class="faq-content">
                <div class="faq-filter">
                    <ul class="filter">
                        <li class="all active"><?php echo($language=="ar")?"جميع الأسئله":"All";?></li>
                        <?php 
                        $terms = get_terms( 'faqs-type',array('hide_empty' => true,));
                        if($terms):
                        foreach($terms as $term):
                        ?>
                        <li data-value="<?php echo $term->slug;?>"><?php echo($language=="ar")? get_field('ar_title',$term) :$term->name;?></li>
                        <?php endforeach; endif;?>
                    </ul>
                </div>
                <div class="tabs-content">
                    <?php 
                    if($terms):
                    foreach($terms as $term):
                        $args = array(
                            'post_type' => 'faqs',
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                            'relation' => 'AND',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'faqs-type',
                                    'field' => 'slug',
                                    'terms' => array($term->slug)
                                )
                                ),
                        );
                        $faqs = get_posts($args);
                    ?>
                    <?php if($faqs): foreach($faqs as $post): setup_postdata($post);?>
                    <div class="tab show <?php echo $term->slug;?>"> 
                        <h3><?php echo($language=="ar")?get_field('ar_title'):get_the_title();?></h3>
                        <div class="content">  
                            <p>
                            <?php echo($language=="ar")?get_field('ar_description'):get_the_content();?>
                            </p>
                        </div>
                        <i class="material-icons open-ques">add</i>
                        <i class="material-icons hide-ques">remove</i>
                    </div>
                        <?php endforeach; endif; wp_reset_postdata();?>
                    <?php endforeach; endif;?>
                </div>
            </div>

        </div>
    </div>

<?php
get_footer();
