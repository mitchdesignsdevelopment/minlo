<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

global $language;
get_header();
?>
    <!-- Start Page Content -->
    <div class="page-content page-contact">
        <div class="container">
            <div class="contact-content">
                <div class="image">
                
                <img class="mob" data-mobile-src="<?php echo get_field('hero_image_mobile');?>" src="<?php echo get_field('hero_image_mobile');?>" alt="Minlo Image" width="100%" height="100%"> 

                <img class="desktop" data-mobile-src="" src="<?php echo get_field('hero_image'); ?>" alt="Minlo Image" width="100%" height="100%">

                </div>
                <div class="content">
                    <span><img src="<?php echo get_template_directory_uri();?>/assets/images/slogan.png" alt="image" width="100%" height="100%"></span>
                    <h1><?php echo($language=="ar")?get_field('hero_title_ar'):get_field('hero_title');?></h1>
                    <p class="head-desc">
                        <?php echo($language=="ar")?get_field('hero_subtitle_ar'):get_field('hero_subtitle');?>
                    </p>
                    <div class="contact-form">
                        <p><?php echo($language=="ar")?get_field('form_title_ar'):get_field('form_title');?></p>
                        <?php echo ($language=="ar")? do_shortcode('[fluentform id="6"]'):do_shortcode('[fluentform id="8"]');?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
