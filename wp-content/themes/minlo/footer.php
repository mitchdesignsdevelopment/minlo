<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package minlo
 */
global $language;
global $wp;

$current_url = home_url( add_query_arg( array(), $wp->request ) );
?>
<!-- here -->
    <!-- Start Page Footer -->                                                      
    <footer class="page-footer">
        <div class="container">
            <div class="footer-con">
                <div class="site-map">
                    <div class="footer-logo">
						<a href="<?php bloginfo('url'); echo($language == 'ar')? '?lang=ar' : ''; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minlo-logo-svg.svg" alt="image"></a>
                    </div>
                    <div class="links">
					<?php		
					if(have_rows('footer_menu','option')):
						while(have_rows('footer_menu','option')): the_row();
							$type = get_sub_field('type');
							$link = get_sub_field('link'); // for column head url
							$level_one = get_sub_field('links');
					?>
                        <div class="links-widget">
                            <h5><?php echo($language=="ar")? get_sub_field('title_ar'):get_sub_field('title')?></h5>
							<?php if($level_one):?>
                            <ul>
								<?php 
								while(have_rows('links')): 
									the_row();
									if($type=="page"):
										$page = get_sub_field('page');
										$page_url = ($language=="ar")?get_the_permalink($page).'/?lang=ar':get_the_permalink($page);
										$page_title = ($language=="ar")?get_field('ar_title',$page):get_the_title($page);
									elseif($type=="category"):
										$page = get_sub_field('category');
										$page_title = ($language=="ar")?get_field('ar_title',$page):$page->name;
										$page_url = ($language=="ar")?get_term_link($page).'/?lang=ar':get_term_link($page);
									else:
										$page = get_sub_field('brand');
										$page_title = ($language=="ar")?get_field('ar_title',$page):$page->name;
										$page_url = ($language=="ar")?get_term_link($page).'/?lang=ar':get_term_link($page);
									endif;
								?>
                                <li><a href="<?php echo $page_url;?>"><?php echo $page_title;?></a></li>
								<?php endwhile;?>
                            </ul>
							<?php endif;?>
                        </div>
					<?php endwhile;endif;?>
                    </div>
                </div>
                <div class="contact-col">
                    <div class="customer-sup">
                        <a href="tel:16016"><?php echo($language=="ar")?'دعم العملاء':'Customer Support';?><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/c-sup-icon.png" alt=""></a>
                    </div>
                    <span><?php echo($language=="ar")?'او اتصل':'or call';?></span>
                    <p><i class="material-icons">phone</i> <a href="tel:+16016">16016</a></p>
                    <ul class="social">
                        <li class="facebook"><a target="_blank" href="<?php echo get_field('facebook_url','option');?>"></a></li>
                        <li class="instagram"><a target="_blank" href="<?php echo get_field('instagram_url','option');?>"></a></li>
                        <li class="youtube"><a target="_blank" href="<?php echo get_field('youtube_url','option');?>"></a></li>
                        <li class="linkedin"><a target="_blank" href="<?php echo get_field('linkedin_url','option');?>"></a></li>
                    </ul>
                </div>
            </div>
            <div class="copyright">
                <div class="top">
                <?php if( $language == 'ar') :?>
								<div class="lang-switcher"><a href="<?php echo $current_url;?>">Browse in English <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/en-flag.png" alt=""></a></div>
							<?php else:?>
								<div class="lang-switcher"><a href="<?php echo $current_url.'/?lang=ar';?>">تصفح بالعربية <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/eg-flag.png" alt=""></a></div>
							<?php endif; ?>
                </div>
                <div class="bottom">
                    <p>®2022 minlo®. All rights reserved - Terms & Conditions - Privacy Policy </p>
                    <a href="https://www.mitchdesigns.com" title="Designed & Developed by MitchDesigns.com" target="_blank">Designed & Developed by MitchDesigns.com</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-3.2.1.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/slick.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/wow.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/main.js"></script>
    <!-- load recaptach only in forms pages -->
<script src="https://www.google.com/recaptcha/api.js"></script>
<!-- load in every page ex product -->
<script rel="preconnect" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js"></script>
<script rel="preconnect" src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
<script rel="preconnect" src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js" ></script>
<script rel="preconnect" src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>

<?php if(is_shop() || is_product_category()): ?>
    <script>
        jQuery(function($) {
            $(window).bind('load',function () {
                get_products_ajax_count();
            });
        });
    </script>
<?php endif; ?>

<?php if(is_singular('product')): ?>
    <script src="https://unpkg.com/video.js/dist/video.min.js"></script>
<?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>
