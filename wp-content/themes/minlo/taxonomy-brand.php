<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package minlo
 */

get_header();
global $language;
$term_id = get_queried_object_id();
$term = get_term_by('id',$term_id,'brand');
$term_slug = $term->slug;
?>

    <!-- Start Page Content -->
    <div class="page-content single-brand">
        <?php 
        if($language=='ar'){
            $mobile_img = get_field('hero_image_mobile_ar',$term);
            $desktop_img = get_field('hero_image_ar',$term);
        }
        else{
            $mobile_img = get_field('hero_image_mobile',$term);
            $desktop_img = get_field('hero_image',$term);
        }
        ?>
        <div class="single-brand-banner">
            <img class="desc" src="<?php echo $desktop_img;?>" alt="">
            <img class="mob" src="<?php echo $mobile_img;?>" alt="">

            <div class="container">
            
            </div>
        </div>
        <div class="brand-content">
            <div class="container">
                <div class="title">
                    <img src="<?php echo get_field('logo',$term);?>" alt="image" class="wow fadeInUp" data-wow-delay=".5s">
                    <span class="wow fadeInUp" data-wow-delay=".6s"><?php echo($language=="ar")? get_field('overview_title_ar',$term):get_field('overview_title',$term);?></span>
                    <h2 class="wow fadeInUp" data-wow-delay=".7s"><?php echo($language=="ar")? get_field('overview_subtitle_ar',$term):get_field('overview_subtitle',$term);?></h2>
                    <p class="wow fadeInUp" data-wow-delay=".8s"><?php echo($language=="ar")? get_field('overview_description_ar',$term):get_field('overview_description',$term);?></p>
                </div>
                <div class="brand-products-container">
				<?php
				$categories = get_terms('product_cat',array('hide_empty' => false));
				$count=1;
				foreach($categories as $category):
                    if ($category->slug === 'uncategorized') {
                        continue; // Skip this category and move to the next one
                    }
					$result = get_posts( array(
						'post_type' => 'product',
						'fields' => 'ids',
						'post_status' => 'publish',
						'post_per_page' => 1,
						'tax_query' =>   array(
							'relation' => 'AND',
							array(
							'taxonomy' => 'product_cat',
							'field' => 'slug',
							'terms' => array($category->slug)
							),
							array(
							'taxonomy' => 'brand',
							'field' => 'slug',
							'terms' => array($term->slug)
							),
						),
					));
					if(count($result)>0){
						
                        $term_img = get_field('image',$category);
			?>
                    <div class="product-row <?php echo($count%2==0)?'flip':'';?>">
                        <div class="image wow fadeInUp" data-wow-delay=".1s" data-wow-offset="0">
                            <img src="<?php echo $term_img;?>" alt="image">
                        </div>
                        <div class="content">
                            <h3 class="wow fadeInUp" data-wow-delay=".2s" data-wow-offset="0"><?php echo($language=="ar")? get_field('ar_title',$category):$category->name;?></h3>
                            <p class="wow fadeInUp" data-wow-delay=".3s" data-wow-offset="0"><?php echo($language=="ar")? get_field('ar_description',$category):get_field('description_en',$category);?></p>
                            <strong><?php echo($language=='ar')?get_field('price_text_ar',$category):get_field('price_text',$category);?></strong>
                            <a href="<?php echo get_term_link($category);echo($language=="ar")?'?lang=ar':'';?>" class="btn wow fadeInUp" data-wow-delay=".4s" data-wow-offset="0"><?php echo($language=="ar")? 'جميع ':'All ';?> <?php echo($language=="ar")? get_field('ar_title',$category):$category->name;?></a>
                        </div>
                    </div>
				<?php
					$count++;
				}
			endforeach;
				?>
                </div>
            </div>
        </div>
		<?php if(have_rows('banners',$term)):?>
        <div class="brand-slider">
			<?php while(have_rows('banners',$term)): the_row(); ?>
            <div class="item">
                <img class="desc" src="<?php echo get_sub_field('image');?>" alt="">
                <img class="mob" src="<?php echo get_sub_field('image_mobile');?>" alt="">
                <div class="text">
                    <h4><?php echo($language=="ar")? get_sub_field('title_ar'):get_sub_field('title');?></h4>
                    <p><?php echo($language=="ar")? get_sub_field('subtitle_ar'):get_sub_field('subtitle');?></p>
                </div>
               
            </div>
          
			<?php endwhile;?>
        </div>
		<?php endif;?>
    </div>
<?php
get_footer();
