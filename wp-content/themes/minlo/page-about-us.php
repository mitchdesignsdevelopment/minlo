<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

global $language;
get_header();
?>
    <!-- Start Page Content -->
    <div class="page-content page-about">

        <div class="container">
            <div class="about-intro">
                
                <div class="content">
                    <h1 class="wow fadeInUp" data-wow-delay=".1s"><?php echo($language=="ar")? get_field('hero_title_ar'):get_field('hero_title');?></h1>
                    <span class="wow fadeInUp" data-wow-delay=".2s"><?php echo($language=="ar")? get_field('hero_subtitle_ar'):get_field('hero_subtitle');?></span>
                    <p class="wow fadeInUp" data-wow-delay=".3s"><?php echo($language=="ar")? get_field('hero_description_ar'):get_field('hero_description');?></p>
                </div>
                <div class="title" style="display: none;">
                    <img src="<?php echo get_field('hero_mobile_logo');?>" alt="image" width="100%" height="100%">
                    <h2><?php echo($language=="ar")? get_field('hero_title_ar'):get_field('hero_title');?></h2>
                    <p><?php echo($language=="ar")? get_field('hero_subtitle_ar'):get_field('hero_subtitle');?></p>
                </div>
                <div class="image wow fadeIn" data-wow-delay=".1s">
                    <img data-mobile-src="<?php echo get_field('hero_image_mobile');?>" src="<?php echo get_field('hero_image');?>" alt="" width="100%" height="100%">
                </div>
                <div class="mobile-content" style="display: none;">
                    <p>
                    <?php echo($language=="ar")? get_field('hero_description_ar'):get_field('hero_description');?>
                    </p>
                </div>
            </div>
        </div>

        <div class="about-elements">
            <!-- Start Hearts Section -->
            <div class="hearts" style="display: none;">
                <div class="hearts-container">
                    <h4><?php echo($language=="ar")? get_field('innovation_title_ar',5):get_field('innovation_title',5);?></h4>
                    <div class="animate-word">
                        <?php
                        if(have_rows('letters_&_words',5)):
                            while(have_rows('letters_&_words',5)): the_row();
                        ?>
                        <div><b><?php echo($language=="ar")? get_sub_field('letter_ar'):get_sub_field('letter');?></b><span></span><p><?php echo($language=="ar")? get_sub_field('word_ar'):get_sub_field('word');?></p></div>
                        <?php endwhile;
                        endif;?>
                    </div>
                </div>
            </div>

            <!-- Start Values Section -->
            <div class="values" style="display: none;">
                <div class="title">
                    <h3><?php echo($language=="ar")? get_field('values_title_ar'):get_field('values_title');?></h3>
                    <p><?php echo($language=="ar")? get_field('values_subtitle_ar'):get_field('values_subtitle');?></p>
                </div>
                <ul class="values-elements">
                <?php
                    if(have_rows('values')):
                        while(have_rows('values')): the_row();
                        $string = ($language=="ar")? get_sub_field('text_ar'):get_sub_field('text');
                        $firstCharacter = substr($string, 0, 1);
                    ?>
                    <li><span class="<?php echo get_sub_field('color');?>"><?php echo $firstCharacter;?></span><?php echo $string;?></li>
                    <?php endwhile;endif;?>
                </ul>
                <div class="image">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/about-decore.png" alt="" width="100%" height="100%">
                </div>
            </div>

            <!-- Start Story Section -->
            <div class="story">
                <div class="title">
                    <h2 class="wow fadeInUp" data-wow-delay=".1s" data-wow-offset="0"><?php echo($language=="ar")?get_field('stories_title_ar'):get_field('stories_title');?></h2>
                    <p class="wow fadeInUp" data-wow-delay=".2s" data-wow-offset="0"><?php echo($language=="ar")?get_field('stories_subtitle_ar'):get_field('stories_subtitle');?></p>
                </div>
                <?php
                if(have_rows('stories')):
                    $count=0.1;
                ?>
                <div class="story-tabs expand">
                <?php while(have_rows('stories')): the_row();?>
                    <div class="tab show wow fadeInUp" data-wow-delay="<?php echo $count;?>s" data-wow-offset="0">
                        <h3><span style="background:<?php echo get_sub_field('color');?>;"><?php echo get_sub_field('year');?></span> <?php echo($language=="ar")?get_sub_field('text_ar'):get_sub_field('text');?></h3>
                        <div class="content">
                            <div class="con">
                                <div class="image">
                                    <img src="<?php echo get_sub_field('image');?>" alt="" width="100%" height="100%">
                                </div>
                                <p><?php echo($language=="ar")?get_sub_field('description_ar'):get_sub_field('description');?>
                                </p>
                            </div>
                        </div>
                        <i class="material-icons open-ques">add</i>
                        <i class="material-icons hide-ques">remove</i>
                    </div>
                <?php $count+=0.1; endwhile;?>
                </div>
            <?php endif;?>
            <?php if($language=="ar"){?>
                <span class="view-timeline" style="display: none;">
                    <i class="material-icons open-timeline">add</i>
                    <i class="material-icons close-timeline">remove</i>
                    عرض الكل
                </span>
                <?php } else{ ?>
                    <span class="view-timeline" style="display: none;">
                    <i class="material-icons open-timeline">add</i>
                    <i class="material-icons close-timeline">remove</i>
                    View All Timeline
                </span>
                <?php } ?>
            </div>

            <!-- Start subscription Section -->
            <div class="subscription-section" style="display: none !important; background-image: url(<?php echo(wp_is_mobile())? get_field('subscription_image_mobile',5):get_field('subscription_image',5);?>);">
                <div class="content">
                    <p><?php echo($language=="ar")? get_field('subscription_description_ar',5):get_field('subscription_description',5);?></p>
               
                    <form class="js-cm-form form" id="subForm" class="js-cm-form" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="5B5E7037DA78A748374AD499497E309E73495739025669BB7D7FF66EFE8E009E36D4F9D8E2BE49AB37D19B4FDCEE7B7D510044BA00526010D13AEDF900F58874">
                <div size="base" class="sc-jzJRlG bMslyb">
                    <div size="small" class="sc-jzJRlG liOVdz">
                        <div>
                        <label class="sc-gzVnrw dEVaGV"><?php echo($language=="ar")?'اشترك لمعرفة اخبار منلو':'Subscribe to our frequent updates';?>
                        </label>
                        <input autoComplete="Email" aria-label="Email" id="fieldEmail" maxLength="200" name="cm-btijhlh-btijhlh" required type="email" class="js-cm-email-input qa-input-email sc-iwsKbI iMsgpL" />
                        </div>
                    </div>
                        <div size="base" class="sc-jzJRlG bMslyb"></div>
                    </div>
                    <button type="submit" class="js-cm-submit-button sc-iAyFgw efTFaG btn"><?php echo($language=="ar")?'اشترك':'Subscribe';?></button>
            </form>
            <script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
