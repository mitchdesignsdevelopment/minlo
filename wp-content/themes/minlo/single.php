<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package minlo
 */

get_header();
?>
    <!-- Start Page Content -->
    <div class="page-content page-news single-news">
        <div class="page-title">
            <span><?php echo get_the_date('F jS, Y');?></span>
            <h1><?php echo get_the_title();?></h1>
        </div>
        <div class="page-intro">
            <img src="<?php echo(wp_is_mobile())? get_field('hero_image_mobile'):get_field('hero_image');?>" alt="Minlo Image"  width="100%" height="100%">
        </div>
        <div class="content"><?php echo($language=="ar")?get_field('ar_description'):get_the_content();?></div>
		<?php  
		$the_querys = get_posts( array('post_type'=>'post','posts_per_page' => 2, 'orderby' => 'rand', 'post_status'=>'published'));
			if ( $the_querys ) {
		?>)
        <div class="news-container">
            <h4>Related Posts</h4>
            <div class="news-list">
			<?php   
			// while ( $the_query->have_posts() ) {
			// 	$the_query->the_post();
			foreach($the_querys as $the_query){
				setup_postdata($the_query);
				$po_img_src = wp_get_attachment_image_src( get_post_thumbnail_id($the_query->ID), 'full' );
				$po_img = $po_img_src[0];
			?>
                <div class="item">
                    <a href="<?php echo get_the_permalink(); echo($language=="ar")?'?lang=ar':'';?>" class="image">
                        <img src="<?php echo $po_img;?>" alt="image"  width="100%" height="100%">
                    </a>
                    <a href="<?php echo get_the_permalink(); echo($language=="ar")?'?lang=ar':'';?>" class="news-title"><?php echo get_the_title();?></a>
                    <span class="date"><?php echo get_the_date('F jS, Y');?></span>
                </div>
			<?php
			}
			?>
            </div>
        </div>
		<?php wp_reset_postdata(); } ?>
    </div>

<?php
get_footer();
