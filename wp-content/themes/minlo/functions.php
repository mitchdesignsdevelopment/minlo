<?php
/**
 * minlo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package minlo
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'minlo_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function minlo_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on minlo, use a find and replace
		 * to change 'minlo' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'minlo', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'minlo' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'minlo_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'minlo_setup' );
function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


add_filter('woocommerce_enqueue_styles', '__return_false');
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function minlo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'minlo_content_width', 640 );
}
add_action( 'after_setup_theme', 'minlo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function minlo_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'minlo' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'minlo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'minlo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function minlo_scripts() {
	wp_enqueue_style( 'minlo-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'minlo-style', 'rtl', 'replace' );

	// wp_enqueue_script( 'minlo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'minlo_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
add_theme_support( 'woocommerce' );
global $language;

if (isset($_GET['lang']) && $_GET['lang'] == 'ar') {
	$language = 'ar';
	$if_en = $language = 'ar';
} else {
    $language = '';
}
// add class to body tag
function my_plugin_body_class($classes) {
	global $language;
    if($language == 'ar') {
		if (($key = array_search('ltr', $classes)) !== false) {
			unset($classes[$key]);  
		}
		$classes[] = 'rtl';
	} else {
		$classes[] = 'ltr';
	}
    return $classes;
} 
add_filter('body_class', 'my_plugin_body_class');

function codex_faqs_init() {
	$args = array(
	'public' => true,
	'label'  => 'FAQS',
	'supports' => array( 'title' , 'editor')

);
	register_post_type( 'faqs', $args );
}
add_action( 'init', 'codex_faqs_init' );
add_action( 'init', 'create_faqs_taxonomy', 0 );
function create_faqs_taxonomy() {
	
	$labels = array(
		'name'              => _x( 'FAQs Types', 'taxonomy general name' ),
		'singular_name'     => _x( 'FAQ', 'taxonomy singular name' ),
		'search_items'      => __( 'Search FAQs Types' ),
		'all_items'         => __( 'All FAQs Types' ),
		'parent_item'       => __( 'Parent FAQ' ),
		'parent_item_colon' => __( 'Parent :FAQ' ),
		'edit_item'         => __( 'Edit FAQ' ),
		'update_item'       => __( 'Update FAQ' ),
		'add_new_item'      => __( 'Add New FAQ' ),
		'new_item_name'     => __( 'New FAQ Name' ),
		'menu_name'         => __( 'FAQs Types' ),
		);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'faqs-type' ),
		);

	register_taxonomy( 'faqs-type', 'faqs' , $args );
}

add_action( 'init', 'create_brand_taxonomy', 0 );
function create_brand_taxonomy() {
	
	$labels = array(
		'name'              => _x( 'Brands', 'taxonomy general name' ),
		'singular_name'     => _x( 'brand', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Brands' ),
		'all_items'         => __( 'All Brands' ),
		'parent_item'       => __( 'Parent brand' ),
		'parent_item_colon' => __( 'Parent :brand' ),
		'edit_item'         => __( 'Edit brand' ),
		'update_item'       => __( 'Update brand' ),
		'add_new_item'      => __( 'Add New brand' ),
		'new_item_name'     => __( 'New brand Name' ),
		'menu_name'         => __( 'Brands' ),
		);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'brand-category' ),
		);

	register_taxonomy( 'brand', 'product' , $args );
}

add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {

    // Check function exists.
    if( function_exists('acf_add_options_page') ) {

        // Register options page.
        acf_add_options_page(array(
            'page_title'    => __('Store Settings'),
            'menu_title'    => __('Store Settings'),
            'menu_slug'     => 'theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false
		));
    }
}

// load more products
add_action( 'wp_ajax_nopriv_get_products_ajax', 'get_products_ajax' );
add_action( 'wp_ajax_get_products_ajax', 'get_products_ajax' );

function get_products_ajax(){
	global $language;
	$action = sanitize_text_field($_POST['fn_action']);
	$count    = intval($_POST['count']);
	$page     = intval($_POST['page']);
	$offset   = ($page) * $count;
	$type = sanitize_text_field($_POST['type']);
	$slug = sanitize_text_field($_POST['slug']);
	$lang = $_POST['lang'];
	$ajaxArgs = array();
	if($lang == "ar"){
		$language = "ar";
	}else{
		$language = "";
	}
	$ajaxArgs = array(
		"post_type" => 'product',
		'post_status' => 'publish',

		"suppress_filters" => false,
		'tax_query'=>array(
			'relation' => 'AND',
			array(
				'taxonomy'         => 'product_visibility',
				'terms'            => array( 'exclude-from-catalog', 'exclude-from-search' ),
				'field'            => 'name',
				'operator'         => 'Not IN',
				'include_children' => false,
			),
		),
	
		);
	if($action == "loadmore"){
		$ajaxArgs["offset"] = $offset;
		$ajaxArgs["posts_per_page"] = $count;
	}else{
		$ajaxArgs["posts_per_page"] = $offset;
	}
	if($type == 'product_cat'){
		$ajaxArgs['product_cat'] = $_POST['slug'];
	}elseif ($type == 'type') {
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'type',
			'field' => 'term_id',
			'terms' => array($_POST['slug'])
			);
	}elseif($type == 'shop'){
	}else{
		$ajaxArgs['tax_query'][] = array(
			'taxonomy' => $type,
			'field' => 'term_id',
			'terms' => array($slug),
		);
	}
	if($_POST['brand'] && count($_POST['brand'])>0){
		$brand = $_POST['brand'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'brand',
			'field' => 'slug',
			'terms' => $brand,
			);
	}
	$ajaxQuery =  new WP_Query($ajaxArgs);
	?>
	<div class="abdo" style="display:none"><?php print_r($ajaxArgs['tax_query']) ?></div>
	<?php
	if($ajaxQuery->have_posts()):
		while ($ajaxQuery->have_posts()) :
			$ajaxQuery->the_post(); 
			$product = wc_get_product( get_the_ID() ); 
			global $product;  
			wc_get_template_part( 'content', 'product' );
		endwhile; wp_reset_postdata(); 
	 endif;
wp_die();
}

// load more products
add_action( 'wp_ajax_nopriv_get_products_ajax_count', 'get_products_ajax_count' );
add_action( 'wp_ajax_get_products_ajax_count', 'get_products_ajax_count' );

function get_products_ajax_count(){
	global $language;
	$count    = intval($_POST['count']);
	$page     = intval($_POST['page']);
	$offset   = ($page) * $count;
	$type = sanitize_text_field($_POST['type']);
	$slug = sanitize_text_field($_POST['slug']);
	$lang = $_POST['lang'];
	$ajaxArgs = array();
	if($lang == "ar"){
		$language = "ar";
	}else{
		$language = "";
	}
	$ajaxArgs = array(
		"post_type" => 'product',
		"posts_per_page"=> -1 ,
		'post_status' => 'publish',
		"suppress_filters" => false,
		"offset" =>$offset,
		'tax_query'=>array(
			'relation' => 'AND',
			array(
				'taxonomy'         => 'product_visibility',
				'terms'            => array( 'exclude-from-catalog', 'exclude-from-search' ),
				'field'            => 'name',
				'operator'         => 'Not IN',
				'include_children' => false,
			),

		),
		
	);
	if($type == 'product_cat'){
		$ajaxArgs['product_cat'] = $_POST['slug'];
	}
	elseif ($type == 'type') {
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'type',
			'field' => 'term_id',
			'terms' => array($_POST['slug'])
			);
	}
	elseif($type == 'shop'){
	}
else{
		$ajaxArgs['tax_query'][] = array(
				'taxonomy' => $type,
				'field' => 'term_id',
				'terms' => array($slug),
		);
	}
	if($_POST['brand'] && count($_POST['brand'])>0){
		$brand = $_POST['brand'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'brand',
			'field' => 'slug',
			'terms' => $brand,
			);
	}	
	$ajaxQuery =  get_posts($ajaxArgs);
		if($ajaxQuery){
			echo count($ajaxQuery);
		}else{
			echo "0";
		}
	wp_die();
}
remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10);
add_action('woocommerce_shop_loop_item_title','fun',10);
function fun()
{
	global $product;
	global $language;
	if($language=="ar") {
		echo '

   		<p class="product-title">' .get_field('ar_title',$product->get_id()).'</p>';
	} else {
		echo '
   		<p class="product-title">' .$product->name.'</p>';
	}
	?>
<?php
}

add_action('wp_ajax_handle_my_file_upload', 'handle_my_file_upload');
add_action('wp_ajax_nopriv_handle_my_file_upload', 'handle_my_file_upload');
function handle_my_file_upload() {
	global $wpdb;
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );
    // will return the attachment id of the image in the media library
	$customer_name = $_POST['customer_name'];
	$rating = $_POST['rating'];
	$email = $_POST['email'];
	$user_id = $_POST['user_id'];
	$sku = $_POST['sku'];
	$note = $_POST['note'];
	$review = $_POST['review'];
	$date = $_POST['date'];
	$files = $_FILES["file"];  
	if($files){
		foreach ($files['name'] as $key => $value) {            
				if ($files['name'][$key]) { 
					$file = array( 
						'name' => $files['name'][$key],
						'type' => $files['type'][$key], 
						'tmp_name' => $files['tmp_name'][$key], 
						'error' => $files['error'][$key],
						'size' => $files['size'][$key]
					); 
					$_FILES = array ("my_file_upload" => $file); 
					foreach ($_FILES as $file => $array) {              
						$attachment_id[] = media_handle_upload($file, 0);
						if (is_wp_error($attachment_id)) {
							http_response_code(400);
							echo 'Failed to upload file.';
						}
					}
				} 
			} 
		
			$attachment_ids = $attachment_id;
	}
    // test if upload succeeded
    if (is_wp_error($attachment_id)) {
        http_response_code(400);
        echo 'Failed to upload file.';
    }
    else {
        http_response_code(200);
		if($_POST['post_id']):
			$product_id = $_POST['post_id'];
		else:
			$product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );
		endif;
		$data = array(
            'comment_post_ID' => $product_id,
            'comment_author' => $customer_name,
            'comment_content' => $review,
            'comment_approved' => 0,
            'comment_type' => 'review',
        );
		if($email){
			$data['comment_author_email'] = $email;
		}
		if($user_id){
			$data['user_id'] = $user_id;
		}
		if($date){
			$data['comment_date'] = date('Y-m-d H:i:s',strtotime($date));
			$data['comment_date_gmt'] = date('Y-m-d H:i:s',strtotime($date));
		}
       $comment_id = wp_insert_comment($data);
	   if($rating):
	   	update_comment_meta( $comment_id, 'rating', $rating);
	   endif;
	   if($attachment_ids):
	   	update_comment_meta( $comment_id, 'attachment_id', $attachment_ids);
	   endif;
	   if($note):
	   	update_comment_meta( $comment_id, 'note', $note);
	   endif;	
	   echo $comment_id;
    }

    // done!
    wp_die();
}


function csvToArray($csvFile){
 
    $file_to_read = fopen($csvFile, 'r');
 
    while (!feof($file_to_read) ) {
        $lines[] = fgetcsv($file_to_read, 1000, ',');
 
    }
 
    fclose($file_to_read);
    return $lines;
}

add_action( 'wp_ajax_nopriv_product_variation_add_to_cart', 'product_variation_add_to_cart' );
add_action( 'wp_ajax_product_variation_add_to_cart', 'product_variation_add_to_cart' );
function product_variation_add_to_cart() {
	global $woocommerce,$language;
	$language = $_POST['lang'];
 if( isset($_POST['pid']) && $_POST['pid'] > 0 ){
	$product_id   = intval($_POST['pid']);
	$quantity     = intval($_POST['qty']);
	$variation_id = intval($_POST['vid']);
	$product_cart_id = ($variation_id && $variation_id !==0)? WC()->cart->generate_cart_id( $product_id,$variation_id ) : WC()->cart->generate_cart_id($product_id);
	$in_cart = WC()->cart->find_product_in_cart( $product_cart_id );
	$case = $_POST['case'];
	if ( ! WC()->cart->is_empty() ) {
		if (  $in_cart ) {
			$cart_status = "updated";
		
			if($case=='minus'){
				foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
					if ($cart_item['product_id'] == $product_id) {
						$woocommerce->cart->set_quantity($cart_item_key, $quantity);
						break;
					}
				}
			}
		}else{
				$cart_status = "added";
		}
	}else{
		$cart_status = "cartEmpty";
	}
	if($case=='remove'){
		if($variation_id && $variation_id !== 0){
			foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
				if ($cart_item['variation_id'] == $variation_id) {
					WC()->cart->remove_cart_item($cart_item_key);
					break;
				}
			}
		}else{
			foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
				if ($cart_item['product_id'] == $product_id) {
					WC()->cart->remove_cart_item($cart_item_key);
					break;
				}
			}
		}
	}
	else{
		if($case=="minus"){
		}
		elseif($case=="add"){
			foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
				if ($cart_item['product_id'] == $product_id) {
					$return['cart_qty'] = $quantity;
					$woocommerce->cart->set_quantity($cart_item_key, $quantity);
					break;
				}
			}
		}
		else{
			if($_POST['product_type']=='variation' || $_POST['product_type']=='variable'){
				$variation_id = (int) $_POST['vid'];
				$meta_data    = $_POST['gift_wrap'];
		
				if(!empty($meta_data)){
				$string = WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, array('gift_wrap'=>$meta_data));
				}
				else{
				$string = WC()->cart->add_to_cart( $product_id, $quantity, $variation_id);
				}
			}
			else{
					$string = WC()->cart->add_to_cart( $product_id, $quantity);
					
				
			}
		}
	}
		$cart_total = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) );
		$return['count'] = WC()->cart->get_cart_contents_count();
		$return['contents'] = update_mini_cart();
		$return['cart_total'] = $cart_total;
		$return['cart_status'] = $cart_status;
		echo json_encode($return);
    }

    die();} // To avoid server error 500
