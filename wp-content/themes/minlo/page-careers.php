<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

global $language;
get_header();
?>
   <!-- Start Page Content -->
   <div class="page-content page-careers">
        <div class="careers-container">
            <div class="page-intro">
                <span><img src="<?php echo($language=="ar")? get_field('hero_subtitle_ar'):get_field('hero_subtitle');?>" alt="" width="100%" height="100%"></span>
                <h1><?php echo($language=="ar")?get_field('hero_title_ar'):get_field('hero_title');?></h1>
                <p><?php echo($language=="ar")?get_field('hero_description_ar'):get_field('hero_description');?></p>
                <img data-mobile-src="<?php echo get_field('hero_image_mobile');?>" src="<?php echo get_field('hero_image');?>" alt="Minlo Image" width="100%" height="100%">
            </div>

            <div class="vacancies">
                <h2><?php echo($language=="ar")?get_field('careers_title_ar'):get_field('careers_title');?></h2>
                <div class="tabs-content">
                    <?php
                    if(have_rows('careers')):
                        while(have_rows('careers')):
                            the_row();
                    ?>
                    <div class="tab"> 
                        <h3><?php echo($language)? get_sub_field('title_ar'):get_sub_field('title');?><span><?php echo($language)? get_sub_field('label_title_ar'):get_sub_field('label_title');?></span></h3>
                        <div class="content">  
                            <?php echo($language)? get_sub_field('description_ar'):get_sub_field('description');?>
                            <a href="mailto:<?php echo get_sub_field('email');?>" target="blank"><?php echo($language)? get_sub_field('excerpt_ar'):get_sub_field('excerpt');?></a>
                        </div>
                        <i class="material-icons open-ques">add</i>
                        <i class="material-icons hide-ques">remove</i>
                    </div>
                    <?php endwhile;endif;?>
                </div>
            </div>

            




        </div>
    </div>
<?php
get_footer();
