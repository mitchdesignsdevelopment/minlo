<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package minlo
 */
global $language;
global $wp;
$current_url = home_url(add_query_arg(array(), $wp->request));
?>
<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<meta name="ajax_url" content="<?php echo admin_url('admin-ajax.php'); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-KTJPRCG');
	</script>
	<!-- End Google Tag Manager -->
	<?php if (is_front_page()) : ?>
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/sass/animate.css">
	<?php endif; ?>
	<?php if (is_singular('product')) : ?>
		<link rel="stylesheet" href="https://vjs.zencdn.net/5-unsafe/video-js.css" />
		<link href="https://unpkg.com/@videojs/themes@1/dist/forest/index.css" rel="stylesheet">
	<?php endif; ?>
	<!-- <link href="<?php //echo get_stylesheet_directory_uri(); 
						?>/assets/sass/main.css?v=<?php //echo time(); 
																							?>" rel="stylesheet"> -->
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/sass/main<?php echo ($language == 'ar') ? '.rtl' : '' ?>.css?v=<?php echo time(); ?>" rel="stylesheet">
	<!-- Google tag (gtag.js) -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=G-J1Z427XC2P"></script>

	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}

		gtag('js', new Date());



		gtag('config', 'G-J1Z427XC2P');
	</script>
	<?php wp_head(); ?>
</head>

<body <?php (is_page('about') || is_page('careers') || is_page('stores') || is_page('frequently-asked-questions')) ? body_class(array('has-bg')) : body_class(); ?> <?php echo (is_singular('product')) ? 'onload="init();"' : ''; ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KTJPRCG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!-- Start Page Header -->
	<header class="page-header wow fadeIn <?php echo (is_product_category() || is_singular('product')) ? 'shop-header' : ''; ?>">
		<div class="container">
			<div class="header-content">
				<div class="left">
					<div class="logo">
						<a href="<?php bloginfo('url');
									echo ($language == 'ar') ? '?lang=ar' : ''; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minlo-logo-svg.svg" alt="image"></a>
					</div>
					<div class="mobile-nav-btns" style="display:none">
						<?php if ($language == 'ar') : ?>
							<div class="lang-switcher"><a href="<?php echo $current_url; ?>">Browse in English <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/en-flag.png" alt=""></a></div>
						<?php else : ?>
							<div class="lang-switcher"><a href="<?php echo $current_url . '/?lang=ar'; ?>">تصفح بالعربية <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/eg-flag.png" alt=""></a></div>
						<?php endif; ?>
						<span class="close-menu"><i class="material-icons">close</i></span>
					</div>
				</div>

				<div class="right">
					<div class="support-menu">
						<ul>
							<li><a href="<?php echo get_the_permalink(357);
											echo ($language == "ar") ? '?lang=ar' : ''; ?>"><?php echo ($language == "ar") ? get_field('ar_title', 357) : get_the_title(357); ?></a></li>
							<li><a href="<?php echo get_the_permalink(470);
											echo ($language == "ar") ? '?lang=ar' : ''; ?>"><?php echo ($language == "ar") ? get_field('ar_title', 470) : get_the_title(470); ?></a></li>
							<li><a href="<?php echo get_the_permalink(500);
											echo ($language == "ar") ? '?lang=ar' : ''; ?>"><?php echo ($language == "ar") ? get_field('ar_title', 500) : get_the_title(500); ?></a></li>
							<?php if ($language == 'ar') : ?>
								<li class="lang-switcher"><a href="<?php echo $current_url; ?>">Browse in English <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/en-flag.png" alt=""></a></li>
							<?php else : ?>
								<li class="lang-switcher"><a href="<?php echo $current_url . '/?lang=ar'; ?>">تصفح بالعربية <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/eg-flag.png" alt=""></a></li>
							<?php endif; ?>
							<li class="customer-sup"><a href="tel:16016"><?php echo ($language == "ar") ? 'دعم العملاء' : 'Customer Support'; ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/c-sup-icon.png" alt=""></a></li>
						</ul>
					</div>
					<ul class="nav-menu">
						<span class="close-mega" style="display: none;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/back-arrow.png" alt=""><?php echo ($language == "ar") ? 'العودة إلى القائمة الرئيسية' : 'Back to Main Menu'; ?></span>
						<?php
						if (have_rows('main_menu', 'option')) :
							while (have_rows('main_menu', 'option')) : the_row();
								$type = get_sub_field('type');
								$level_one = get_sub_field('level_one_subcategories');
								if ($type == "page") :
									$page = get_sub_field('page_link'); // for page (id)
									$page_title = ($language == "ar") ? get_field('ar_title', $page) : get_the_title($page);
									$page_url = ($language == "ar") ? get_the_permalink($page) . '/?lang=ar' : get_the_permalink($page);
								elseif ($type == "brand") :
									$page = get_sub_field('collection_link'); //for brand (object)
									$page_title = ($language == "ar") ? get_field('ar_title', $page) : $page->name;
									$page_url = ($language == "ar") ? get_term_link($page) . '/?lang=ar' : get_term_link($page);
								else :
									$page = get_sub_field('link'); // for category (object)
									$page_title = ($language == "ar") ? get_field('ar_title', $page) : $page->name;
									$page_url = ($language == "ar") ? get_term_link($page) . '/?lang=ar' : get_term_link($page);
								endif;
						?>
								<?php if ($level_one) : ?>
									<li class="has-menu">
										<a href="<?php echo $page_url; ?>" class="desktop-link"><?php echo ($language == "ar") ? get_sub_field('coulmn_title_ar') : get_sub_field('coulmn_title'); ?></a>
										<span style="display: none;"><?php echo ($language == "ar") ? get_sub_field('coulmn_title_ar') : get_sub_field('coulmn_title'); ?></span>
										<div class="mega-menu">
											<h4 style="display: none;"><?php echo ($language == "ar") ? get_sub_field('coulmn_title_ar') : get_sub_field('coulmn_title'); ?></h4>
											<ul class="dropdown-menu <?php echo ($type == "brand") ? 'brands' : ''; ?>">
												<?php
												while (have_rows('level_one_subcategories')) : the_row();
													if (get_sub_field('type') == "brand") {
														$obj = get_sub_field('brand');
														$term_img = get_field('logo', $obj);
													} else {
														$obj = get_sub_field('category');
														$thumb_id = get_woocommerce_term_meta($obj->term_id, 'thumbnail_id', true);
														$term_img = wp_get_attachment_url($thumb_id);
													}
												?>
													<li><a href="<?php echo get_term_link($obj);
																	echo ($language == "ar") ? '?lang=ar' : ''; ?>">
															<img src="<?php echo $term_img; ?>" alt="">
															<span><?php echo ($language == "ar") ? get_field('ar_title', $obj) : $obj->name; ?><i class="material-icons">chevron_right</i></span>
														</a></li>
												<?php endwhile; ?>
											</ul>
										</div>
									</li>
								<?php else : ?>
									<li><a href="<?php echo $page_url; ?>"><?php echo $page_title; ?></a></li>
								<?php endif; ?>
						<?php endwhile;
						endif; ?>
					</ul>
				</div>
			</div>
			<div class="mobile-header" style="display: none;">
				<div class="logo">
					<a href="<?php bloginfo('url');
								echo ($language == 'ar') ? '?lang=ar' : ''; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minlo-logo-svg.svg" alt="image"></a>
				</div>
				<div class="mobile-nav-btns">
					<?php if ($language == 'ar') : ?>
						<div class="lang-switcher"><a href="<?php echo $current_url; ?>">Browse in English <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/en-flag.png" alt=""></a></div>
					<?php else : ?>
						<div class="lang-switcher"><a href="<?php echo $current_url . '/?lang=ar'; ?>">تصفح بالعربية <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/eg-flag.png" alt=""></a></div>
					<?php endif; ?>
					<span class="trigger-menu"><i class="material-icons">menu</i></span>
				</div>
			</div>
		</div>
	</header>