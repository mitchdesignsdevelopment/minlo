<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package minlo
 */

get_header(); 
global $language;
?>
  <!-- Start Page Content -->
<div class="page-content">

    <!-- Start Intro hero -->
    <div class="intro">
        <div class="hero-banner wow fadeIn">
            <?php
                if(have_rows('banners')):
                    while(have_rows('banners')): the_row();
            ?>
            <div class="item" style="background:<?php echo get_sub_field('banner_color_picker');?>;">  
                <div class="item-container">
                    <div class="content <?php echo(get_sub_field('banner_text_color'))?'white-text':'black-text';?>">
                        <div class="icon wow fadeInUp" data-wow-delay="4s">
                            <img src="<?php echo get_sub_field('banner_icon');?>" alt="" width="100%" height="100%">
                        </div>
                        <h2 class="wow fadeInUp" data-wow-delay="4.3s"><?php echo($language=="ar")? get_sub_field('banner_title_ar'):get_sub_field('banner_title');?></h2>
                        <h3 class="wow fadeInUp" data-wow-delay="4.6s"><?php echo($language=="ar")? get_sub_field('banner_subtitle_ar'):get_sub_field('banner_subtitle');?></h3>
                    </div>
                    <div class="image wow fadeInUp" data-wow-delay="1s" data-wow-duration="1.5s">
                        <!-- adding fetchpriority="high" LCP prolem -->
                        <img fetchpriority="high" data-mobile-src="<?php echo get_sub_field('banner_mobile');?>" src="<?php echo get_sub_field('banner');?>" alt="" width="100%" height="100%">
                    </div>
                </div> 
            </div>
            <?php endwhile;endif;?>
        </div>
    </div>

    <!-- Start Brands & Products Section -->
    <div class="brands-products">
        <!-- Start Products Section -->
        <div class="products-nav">
            <div class="title" style="display: none;">
                <h4><?php echo($language=="ar")?'منتجاتنا':'Our Products';?></h4>
                <p><img src="assets/images/slogan.png" alt="" width="100%" height="100%"></p>
            </div>
            <?php
                $featured_categories = get_field('featured_categories');
                if($featured_categories):             
            ?>
            <ul class="products-nav-con">
                <?php
                $count = 1.5;
                    foreach($featured_categories as $featured_category){
                    $thumb_id = get_woocommerce_term_meta( $featured_category->term_id, 'thumbnail_id', true );
                    $term_img = wp_get_attachment_url(  $thumb_id );
                ?>
                <li class="wow fadeInUp" data-wow-delay="<?php echo $count;?>s"><a href="<?php echo get_term_link($featured_category);echo($language=="ar")?'?lang=ar':'';?>">
                    <?php if($term_img):?>
                    <div class="image">
                        <img src="<?php echo $term_img;?>" alt="" width="100%" height="100%">
                    </div>
                    <?php endif;?>
                    <span> <?php echo($language=="ar")? get_field('ar_title',$featured_category):$featured_category->name;?> <i class="material-icons">chevron_right</i></span>
                </a></li>
                <?php $count+=0.4; } ?>
            </ul>
            <?php endif;?>
            </div>
        <?php $brands = get_terms('brand',array('hide_empty' => false));?>
        <!-- Start Brands Section -->
        <?php if($brands): $count=1;?>
        <div class="our-brands">
            <div class="brands-container">
                <div class="title">
                    <h4 class="wow fadeInUp" data-wow-offset="40"><?php echo($language=="ar")? get_field('brands_title_ar'):get_field('brands_title');?></h4>
                    <p class="wow fadeInUp" data-wow-offset="20"><?php echo($language=="ar")? get_field('brands_subtitle_ar'):get_field('brands_subtitle');?></p>
                </div>
                <?php foreach($brands as $brand):?>
                <div class="brand-widget <?php echo($count%2==0)?'flip':'';?> wow fadeInUp" data-wow-offset="30" data-wow-duration="2.5s">
                    <div class="image">
                        <a href="<?php echo get_term_link($brand);echo($language=="ar")?'?lang=ar':'';?>">
                            <img src="<?php echo get_field('image',$brand);?>" alt="" width="100%" height="100%">
                        </a>
                    </div>
                    <div class="content">
                        <img class="wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1s" data-wow-offset="10" src="<?php echo get_field('logo',$brand);?>" alt="" width="100%" height="100%">
                        <p class="wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="1s" data-wow-offset="10"><?php echo($language=="ar")?get_field('ar_description',$brand):$brand->description;?></p>
                    </div>
                </div>
                <?php $count++; endforeach;?>
            </div>
        </div>
        <?php endif;?>
    </div>
    <!-- Start Hearts Section -->
    <div class="hearts">
        <div class="hearts-container">
            <h4 class="wow fadeInUp" data-wow-offset="-20"><?php echo($language=="ar")? get_field('innovation_title_ar'):get_field('innovation_title');?></h4>
            <div class="animate-word">
                <?php
                if(have_rows('letters_&_words')): $count=0.1;
                    while(have_rows('letters_&_words')): the_row();
                ?>
                <div class="wow fadeInUp" data-wow-delay="<?php echo $count?>s" data-wow-offset="-20"><b><?php echo($language=="ar")? get_sub_field('letter_ar'):get_sub_field('letter');?></b><span></span><p><?php echo($language=="ar")? get_sub_field('word_ar'):get_sub_field('word');?></p></div>
                <?php $count+=0.2; endwhile;
                endif;?>
            </div>
        </div>
    </div>

    <!-- Start About Section -->
    <div class="about-section">
        <div class="container">
            <div class="about-con">
                <div class="content">
                    <!-- here -->
                    <div class="content-text">
                        <h4 class="wow fadeInUp" data-wow-offset="0" data-wow-delay="2.3s"><?php echo($language=="ar")? get_field('about_title_ar'):get_field('about_title');?></h4>
                        <div  class="wow fadeInUp" data-wow-delay="2.5s">
                            <?php echo($language=="ar")? get_field('about_description_ar'):get_field('about_description');?>
                        </div>
                        <a class="wow fadeInUp" data-wow-offset="0" data-wow-delay="2.8s" href="<?php echo get_field('button_link');echo($language=="ar")?'?lang=ar':'';?>"><?php echo($language=="ar")? get_field('button_text_ar'):get_field('button_text');?> <i class="material-icons">arrow_forward</i></a>
                    </div>
                </div>
                <div class="image wow fadeInRight"  data-wow-offset="10" data-wow-delay="2s">
                    <img class="wow slideInLeft"  data-wow-offset="0" data-wow-delay="2s" data-wow-duration="2s" src="<?php echo get_field('about_image');?>" alt="image" width="100%" height="100%">
                </div>
            </div>  
        </div>
    </div>

    <!-- Start subscription Section -->
    <div class="subscription-section" style="display: none;">
        <div class="content">
            <p><?php echo($language=="ar")? get_field('subscription_description_ar'):get_field('subscription_description');?></p>
           
            <form class="js-cm-form form" id="subForm" class="js-cm-form" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="5B5E7037DA78A748374AD499497E309E73495739025669BB7D7FF66EFE8E009E36D4F9D8E2BE49AB37D19B4FDCEE7B7D510044BA00526010D13AEDF900F58874">
                <div size="base" class="sc-jzJRlG bMslyb">
                    <div size="small" class="sc-jzJRlG liOVdz">
                        <div>
                        <label class="sc-gzVnrw dEVaGV"><?php echo($language=="ar")?'اشترك لمعرفة اخبار منلو':'Subscribe to our frequent updates';?>
                        </label>
                        <input autoComplete="Email" aria-label="Email" id="fieldEmail" maxLength="200" name="cm-btijhlh-btijhlh" required type="email" class="js-cm-email-input qa-input-email sc-iwsKbI iMsgpL" /></div></div><div size="base" class="sc-jzJRlG bMslyb"></div></div><button type="submit" class="js-cm-submit-button sc-iAyFgw efTFaG btn"><?php echo($language=="ar")?'اشترك':'Subscribe';?></button></form>
            <script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
        </div>
        <div class="image">
            <img src="<?php echo(wp_is_mobile())? get_field('subscription_image_mobile'):get_field('subscription_image');?>" alt="image" width="100%" height="100%">
        </div>
    </div>
</div>
    <?php    



    ?>

<?php
get_footer();
